# Scripts

## Encrypting a value
This repository is open. When the chart are published for use by our clusters, it needs to be open as well.
In cases where we need to use secret values, we use the `encryptAES` and `decryptAES` functions and we check the encrypted value into the repository so it can be open but still secret.

To prepare an encrypted string we use the builtin `encryptAES` function to ensure it will be 100% compatible with `decryptAES` like so:

`helm template --generate-name ./encrypt --set valueToEncrypt=<valueToEncrypt> --set secret=<encryptionPassword>`

_be cautious using `--set` with weird strings, as helm does auto-coercion magick; the safest mechanism is to put the two strings in a "values.yaml" and use `-f`_

Example:
```shell
> helm template --generate-name ./encrypt --set valueToEncrypt=foobar --set secret=fakesecret
---
# Source: encrypt/templates/out.yaml
encrypted: 7p2PQw9iI+7IhcWNoTZ5bLnRtAGxmqSN4NrzRdSBqF0=

# Test decrypting. These should be equal
foobar: foobar
```

You may use the encrypted value in this open repo to be decrypted at `apply` time.

**DO NOT CHECK IN THE `secret`**
