# Open Raven Splunk Implementation

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| dbConnect.configMapName | string | `"db-connect"` |  |
| fullnameOverride | string | `nil` |  |
| global.awsRegion | string | `nil` |  |
| global.groupId | string | `nil` | used to build the full the IAM Role name |
| global.openravenIngressHostname | string | `nil` |  |
| ingress.annotations."nginx.ingress.kubernetes.io/auth-response-headers" | string | `"x-auth-request-user, x-auth-request-email, x-auth-request-groups, x-auth-request-access-token"` |  |
| ingress.annotations."nginx.ingress.kubernetes.io/auth-signin" | string | `"https://$host/oauth2/start?rd=$escaped_request_uri"` |  |
| ingress.annotations."nginx.ingress.kubernetes.io/auth-url" | string | `"https://$host/oauth2/auth"` |  |
| ingress.annotations."nginx.ingress.kubernetes.io/configuration-snippet" | string | `"auth_request_set $user $upstream_http_x_auth_request_email;\nproxy_set_header Remote-User '$user';\nauth_request_set $groups $upstream_http_x_auth_request_groups;\nproxy_set_header Remote-Groups '$groups';\nauth_request_set $bearer_token $upstream_http_x_auth_request_access_token;\nproxy_set_header authorization 'Bearer $bearer_token';\n"` |  |
| ingress.enabled | bool | `true` |  |
| ingress.hosts[0].endpoints[0].kind | string | `"standalone"` | the `kind:` of the CRD whose `Service` one wishes to target; this has a relationship with the `splunk.name` too |
| ingress.hosts[0].endpoints[0].path | string | `"/splunk"` | `splunk.searchHead.rootEndpoint` needs to be set the same |
| ingress.hosts[0].host | string | `nil` |  |
| ingress.tls | list | `[]` |  |
| kiamAccountId | string | `nil` | the Role's AWS Account ID for that kiamRole when running under EKS |
| kiamRoleSuffix | string | `nil` | the Role Name **Suffix** the Pods should assume in order to access S3; it will be appended to "orvn-${groupId}-" to form the full Role Name |
| licenseMasterSecretName | string | `"splunk-license-master"` | this Secret is created by the splunk-operator chart and is static, so **DO NOT CHANGE IT** it contains one key `default.yml` which is why it is referenced as `/mnt/.../default.yml` |
| nameOverride | string | `nil` |  |
| pgsql.connectionName | string | `"pgsql"` | the name of the `connection` used in the openraven/splunk-apps repo, so **DO NOT CHANGE IT** |
| pgsql.identityName | string | `"pgsql"` | The name of the "identity" created by `identities.sh` (as bundled into the splunk-java image) thus, it is in values.yaml only for reuse, so **DO NOT CHANGE IT** |
| pgsql.jdbcSecretName | string | `"jdbc-env"` | used by `identities.sh` and expected to contain `SPRING_DATASOURCE_USERNAME` style keys |
| pgsql.target | string | `"postgresql://localhost:5432"` | the same form as `$SQITCH_TARGET` |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.create | bool | `true` |  |
| serviceAccount.name | string | `nil` |  |
| splunk.bucketName | string | `nil` | the S3 SmartStore bucket name |
| splunk.clusterMaster.resources.limits.memory | string | `"6Gi"` |  |
| splunk.clusterMaster.resources.requests.cpu | int | `2` |  |
| splunk.clusterMaster.resources.requests.memory | string | `"6Gi"` |  |
| splunk.hwf.image.registry | string | `"160390125595.dkr.ecr.us-west-2.amazonaws.com"` |  |
| splunk.hwf.image.repository | string | `"openraven/open/splunk-java"` |  |
| splunk.hwf.image.tag | string | `"334045449"` |  |
| splunk.hwf.resources.limits.memory | string | `"8Gi"` |  |
| splunk.hwf.resources.requests.cpu | int | `2` |  |
| splunk.hwf.resources.requests.memory | string | `"8Gi"` |  |
| splunk.indexer.replicas | int | `3` |  |
| splunk.indexer.resources.limits.memory | string | `"12Gi"` |  |
| splunk.indexer.resources.requests.cpu | int | `4` |  |
| splunk.indexer.resources.requests.memory | string | `"12Gi"` |  |
| splunk.name | string | `"orvn"` | the `name:` for *almost* every custom resource |
| splunk.port | int | `8000` | used in the Ingress to point to the Service port |
| splunk.searchHead.configMapName | string | `"search-head-config"` |  |
| splunk.searchHead.resources.limits.memory | string | `"12Gi"` |  |
| splunk.searchHead.resources.requests.cpu | int | `2` |  |
| splunk.searchHead.resources.requests.memory | string | `"12Gi"` |  |
| splunk.searchHead.rootEndpoint | string | `"/splunk"` | the URI the search head listens on, so make sure to update the `ingress.endpoints.path` as well |
| splunkApps.bucket | string | `"openraven-deploy-artifacts"` |  |
| splunkApps.prefix | string | `"splunk-app"` | BE CAREFUL, this has an implicit leading "/" |
