#!/usr/bin/env bash

trace_on() {
  if [[ -n ${TRACE:-} ]]; then
    set -x
  fi
}

# pretend they set the TRACE env-var if this was invoked with -x
# it makes the trace_on logic simpler
if [[ :${SHELLOPTS}: == *:xtrace:* ]]; then
  TRACE=1
fi

trace_on

API_URL="${API_URL:-https://api.openraven.com/api}"

# (1) make them visible to shellcheck (2) make them explode early if not set
ACM_CERT_ARN=${ACM_CERT_ARN?}
export ACM_CERT_ARN
AWS_REGION=${AWS_REGION?}
# for some reason awscli does not honor AWS_REGION
AWS_DEFAULT_REGION=${AWS_REGION?}
export AWS_DEFAULT_REGION
AWS_CFN_STACK_NAME=${AWS_CFN_STACK_NAME?}
GROUP_ID=${GROUP_ID?}
INSTALL_EMAIL=${INSTALL_EMAIL?}
if [[ -z "${HELM_REPO_URL:-}" ]]; then
  HELM_REPO_URL="https://${HELM_S3_BUCKET?}.s3.amazonaws.com"
fi
if [[ -z "${HELMFILE_YAML_URL:-}" ]]; then
  HELMFILE_YAML_URL="${HELM_REPO_URL}/${HELM_S3_KEY?}"
fi
# this one is optional
CFN_SIGNAL_NAME=${CFN_SIGNAL_NAME:-}

ACTIVATION_PAYLOAD_FN=activationPayload.json
# the local filename into which $HELMFILE_YAML_URL is written
HELMFILE_FN=helmfile.yaml
# be aware this needs to match the replicator chart in helmfiles
JDBC_SECRET_NAMESPACE='kube-system'
JDBC_SECRET_NAME='jdbc-env'
# make this one visible to helmfile for use in "envFrom:"
export JDBC_SECRET_NAME

# its seems awscli doesn't know to look for it or something
# and we need this early so we can sniff out the JDBC env-vars
if [[ -n ${AWS_ROLE_ARN:-} ]] && [[ -z ${AWS_WEB_IDENTITY_TOKEN_FILE:-} ]]; then
  awitf=/run/secrets/eks.amazonaws.com/serviceaccount/token
  # be sure to use -e as the /token file is a symlink
  if [[ -e $awitf ]]; then
    export AWS_WEB_IDENTITY_TOKEN_FILE=$awitf
  else
    echo 'You have set AWS_ROLE_ARN without AWS_WEB_IDENTITY_TOKEN_FILE, which is unlikely to end well' >&2
    sleep 5
  fi
  unset awitf
fi

echo 'Current AWS Identity:' >&2
{ aws sts get-caller-identity | tee sts.json; } || true
if grep ':i-[0-9a-f]' sts.json; then
  echo 'Your IRSA did not pan out, fatal' >&2
  sleep 30
  exit 1
fi

if [[ -z "${AWS_REGION_CSV:-}" ]]; then
  # prioritize us regions since (for now) things are more likely to be there
  AWS_REGION_CSV=$(aws ec2 describe-regions | jq -r '
( [.Regions[].RegionName | select(index("us-"))]
+ [.Regions[].RegionName | select(index("us-")|not)]
) | join(",")
')
fi

if [[ -z "${SPRING_DATASOURCE_URL:-}" ]]; then
  echo 'Stalling until the outer stack is stable ...' >&2
  # regrettably, "wait" distinguishes between _update_ complete and _create_ complete :-(
  for _ in $(seq 1 600); do
    if aws cloudformation describe-stacks --stack-name "$AWS_CFN_STACK_NAME" \
        --query 'Stacks[*].StackStatus' --output text | grep -q COMPLETE; then
      break
    fi
    sleep 5
  done
  cfn_to_jdbc_env() {
    aws cloudformation describe-stacks --stack-name "$AWS_CFN_STACK_NAME" > /tmp/stack.json
    if ! jq -r '[.Stacks[0].Outputs[] | {key: .OutputKey, value: .OutputValue}]
      | from_entries | {
        PGHOST: .PgHost,
        PGPORT: .PgPort,
        PGUSER: .PgUser,
        PGPASSWORD: .PgStackUuid,
        PGDATABASE: .PgDatabase
      } | to_entries[]
      | select(.value)
      | "export \(.key)=\(.value)"
      ' < /tmp/stack.json; then
        echo 'BOGUS stack.json: ' >&2
        cat /tmp/stack.json >&2
        return 1
    fi
  }
  eval "$(cfn_to_jdbc_env)"
  if [[ -z "${PGUSER:-}" ]]; then
    echo 'PG env var extract did not pan out' >&2
    cat /tmp/stack.json || true
    exit 1
  fi
  export SPRING_DATASOURCE_URL="jdbc:postgresql://${PGHOST}:$PGPORT/$PGDATABASE"
  export SPRING_DATASOURCE_USERNAME=$PGUSER
  export SPRING_DATASOURCE_PASSWORD="$PGPASSWORD"
fi

# these are the names we grab from the environment and create a Secret containing them
JDBC_ENV_VAR_NAMES='SPRING_DATASOURCE_URL SPRING_DATASOURCE_USERNAME SPRING_DATASOURCE_PASSWORD'
JDBC_ENV_VAR_NAMES="$JDBC_ENV_VAR_NAMES SQITCH_TARGET SQITCH_USERNAME SQITCH_PASSWORD"
# these are because jira-integration now depends upon them
JDBC_ENV_VAR_NAMES="$JDBC_ENV_VAR_NAMES PGUSER PGPASSWORD PGHOST PGPORT PGDATABASE"

SPRING_DATASOURCE_URL=${SPRING_DATASOURCE_URL?}
SPRING_DATASOURCE_USERNAME=${SPRING_DATASOURCE_USERNAME?}
SPRING_DATASOURCE_PASSWORD=${SPRING_DATASOURCE_PASSWORD?}
# thankfully their target syntax is the same as the JDBC URL just without the leading "jdbc:"
SQITCH_TARGET=${SPRING_DATASOURCE_URL#jdbc:}
# these must be exported for "printenv" to see them below
export SQITCH_TARGET
SQITCH_USERNAME=${SPRING_DATASOURCE_USERNAME?}
export SQITCH_USERNAME
SQITCH_PASSWORD=${SPRING_DATASOURCE_PASSWORD?}
export SQITCH_PASSWORD

# the kubernetes NS into which the jdbc secret is initially replicated
JDBC_NAMESPACE_CSV='aws,cluster-upgrade,dmap,splunk,ui'

# it appears EKS is installing a "gp2" using the /aws-ebs provisioner
# and they mark it as the default, which causes _our_ default in EBS CSI
# to make double defaults
if kubectl get -o wide storageclass | grep -q kubernetes.io/aws-ebs; then
  # trust me, shellcheck, I'm a professional
  # shellcheck disable=SC2046
  kubectl delete $(kubectl get -o name storageclass)
fi

# there here is a development-time concession to keep from having to roll a new chart to test changes
patch_fn=/tmp/patch.sh
if [[ -z "${WITHOUT_PATCH:-}" ]] && curl -fsSLo "$patch_fn" "${HELM_REPO_URL}/${AWS_CFN_STACK_NAME}/helmfile0.patch.sh"; then
  # dev time gets unconditional +x
  set -x
  export WITHOUT_PATCH=1
  # no, of course it doesn't exist shellcheck get a grip
  # shellcheck disable=SC1090
  source "$patch_fn"
  exit $?
fi

cfn_signal() {
  local cfs
  cfs=/opt/aws/apitools/cfn-init/bin/cfn-signal
  signal_rc='0'
  if [[ ! -x "$cfs" ]]; then
    return 0
  fi
  $cfs --region="$AWS_REGION" \
    --stack="$AWS_CFN_STACK_NAME" \
    --resource="$CFN_SIGNAL_NAME" \
    --success=true \
    --exit-code="$signal_rc"
}

zk_cli() {
  zookeepercli --servers kafka-zookeeper.kafka.svc.cluster.local:2181 "$@"
}

zk_create_config_and_verify() {
  local zk_key="/config/application/openraven$1"
  local zk_value="$2"
  local zk_get_value
  # unlike cfn-pivot's version, there is a non-zero chance ZK is already populated
  # so leave the values in place if so
  if zk_cli -c exists "$zk_key"; then
    return 0
  fi
  zk_cli -c creater "$zk_key" "$zk_value"
  zk_cli -c set     "$zk_key" "$zk_value"
  zk_get_value="$(zk_cli -c get "$zk_key")"
  if [[ "x${zk_get_value}" != "x${zk_value}" ]]; then
    return 1
  fi
  return 0
}

send_outcome_post() {
  local outcome="$1"
  if [[ -n "${SKIP_OUTCOME_EMAIL:-}" ]]; then
    return
  fi
  for _ in 1 2 3; do
    if curl -fsSH 'content-type: application/json' \
        --data-binary '{"cluster-url": "https://'"${OPENRAVEN_INGRESS_HOSTNAME}"'"}' \
        "${API_URL}/account/activate/${outcome}/${GROUP_ID}"
    then
      break
    fi
    sleep 5
  done
}

_activation_payload() {
  # the cluster-url is the **Ingress hostname**, but activation will create a new domain
  # and CNAME that cluster-url to "TheNewDnsName.the-cluster-domain"
  jq -n \
    --arg openraven_ingress_hostname "$INGRESS_ELB" \
    --arg deployChannel "$HELM_S3_BUCKET" \
    --arg activationPayload "$ACTIVATION_PAYLOAD" \
  '{
    "cluster-url": "https://\($openraven_ingress_hostname)",
    "createDomain": true,
    "deployChannel": $deployChannel
  } + ($activationPayload | fromjson)'
  # thankfully "+" is `null` safe, so {} + null == {}
  # as will be the case if $EXTRA_PAYLOAD does not contain activationPayload
}

activate_cluster() {
  local CURL_USER_AGENT
  CURL_USER_AGENT="helmfile0 (deploy-channel=${HELM_S3_BUCKET}; email=${INSTALL_EMAIL}; region=${AWS_REGION})"
  if [[ ! -e "$ACTIVATION_PAYLOAD_FN" ]]; then
    curl -vo "$ACTIVATION_PAYLOAD_FN" \
      -A "$CURL_USER_AGENT" \
      -H 'content-type: application/json;charset=utf-8' \
      --data-binary "$(_activation_payload)" \
      "${API_URL}/account/activate/${GROUP_ID}" 2>act.err
    echo
    cat act.err
    echo
    # re-implement "--fail" because there does not appear to be any "--fail-but-show-me-anyway"
    grep -qi '< HTTP/[0-9.]* 2[0-9]' act.err
    echo
    cat "$ACTIVATION_PAYLOAD_FN"
    # cheat and take advantage of the fact that we're running as cluster-admin
    kubectl -n kube-system create secret generic helmfile0 \
      --from-file=activation.json="$ACTIVATION_PAYLOAD_FN" || true
  fi
}

_unpack_activation_payload() {
  # keep us from having to put "export " around everything
  echo 'set -a;'
  jq -r '
  [
    {
      OPENRAVEN_INGRESS_HOSTNAME: .route53.domainName,
      FRONTEND_CLIENT_ID: .feClientId,
      SERVICE_CLIENT_ID: .beClientId,
      SERVICE_CLIENT_SECRET: .beClientSecret,
      ADMIN_CLIENT_SECRET: .adminClientSecret,
      ADMIN_CLIENT_ID: .adminClientId,
      CLUSTER_NAME: .clusterName,
      COOKIE_SECRET: .cookieSecret,
    }
    | to_entries[] | "\(.key)=\"\(.value)\""
  ] | join(";")
  ' < "$ACTIVATION_PAYLOAD_FN"
  echo 'set +a;'
}

install_ingress() {
  local git_url git_ref git_dir f
  # feel free to try and regex this out if it bothers you
  git_dir='helmfile.d'
  git_url=$(sed -nEe 's/.*git::(https:.+?)\.git.*/\1/p' "$HELMFILE_FN")
  git_ref=$(sed -nEe 's/.*"HELMFILE_GIT_REF"[^"]+"([^"]+)".*/\1/p' "$HELMFILE_FN")
  # regrettably, we need the whole directory now because of the ./values references
  # and git clone -b doesn't honor pure shas
  curl -fsSL "${git_url}/-/archive/${git_ref}.tar" | tar -xf - -C /tmp
  # the archive is ${project_name}-${git_ref}/helmfile.d
  cd /tmp/*"${git_ref}/$git_dir" || exit 1

  # so, this nonsense is because database-schema boots up the kong schema,
  # but database-schema needs the secret replicator in place
  # DO NOT install kong2 as we have no clientId right now
  for f in replicator.yaml database-schema.yaml kong0.yaml kong1.yaml; do
    helmfile --log-level debug --no-color -f "./${f}" apply --wait --wait-for-jobs
  done
  cd - || exit 1
}

######################################
### Main
######################################

mkdir -p /helmfiles
cd       /helmfiles || exit 1

if [[ -n "$CFN_SIGNAL_NAME" ]]; then
  # tell CFN we have at least gotten this far
  cfn_signal
fi

# this silliness is that sometimes the NatGw takes a while to heat up
# causing Internet connectivity to :fu:
now=$(date +%s)
wait_seconds=120
poll_timeout=$(( now + wait_seconds ))
if [[ ! -e "$HELMFILE_FN" ]]; then
  while [[ $(date +%s) -lt $poll_timeout ]]; do
    if ! curl -fsSLo "$HELMFILE_FN" "$HELMFILE_YAML_URL"; then
      echo 'sleeping 5 for helmfile.yaml retry ...' >&2
      sleep 5
      continue
    fi
    break
  done
fi

cfn_json_fn=cfn.json
aws --region "$AWS_REGION" cloudformation describe-stacks --stack-name "$AWS_CFN_STACK_NAME" >"$cfn_json_fn"
if [[ -z ${EXTRA_PAYLOAD:-} ]]; then
  EXTRA_PAYLOAD=$(jq -r '.Stacks[0].Parameters[] | select(.ParameterKey=="ExtraPayload") | .ParameterValue' "$cfn_json_fn")
fi

ACTIVATION_PAYLOAD=$(jq -r .activationPayload <<JSON
$EXTRA_PAYLOAD
JSON
)
# BE AWARE: if EXTRA_PAYLOAD is "", then -r of anything is _the empty string_
# and not "null", but $(jq --arg activationPayload '') below will produce an error
# so we have to coerce this as if they provided "{}" and then -r emitted "null"
ACTIVATION_PAYLOAD="${ACTIVATION_PAYLOAD:-null}"

if [[ -z "${CLUSTER_TYPE:-}" ]]; then
  CLUSTER_TYPE=$(jq -r .cluster_type <<JSON
$EXTRA_PAYLOAD
JSON
  )
fi
CLUSTER_TYPE=${CLUSTER_TYPE:-saas}

MARKETPLACE_PRODUCT_CODE=$(jq -r .marketplace_product_code <<JSON
$EXTRA_PAYLOAD
JSON
)
export MARKETPLACE_PRODUCT_CODE

MARKETPLACE_CUSTOMER_IDENTIFIER=$(jq -r .marketplace_customer_identifier <<JSON
$EXTRA_PAYLOAD
JSON
)
export MARKETPLACE_CUSTOMER_IDENTIFIER

JDBC_ENV_FN=./jdbc-env
for n in $JDBC_ENV_VAR_NAMES; do
  echo -n "${n}=" >> "$JDBC_ENV_FN"
  printenv "$n"   >> "$JDBC_ENV_FN"
done
unset JDBC_ENV_VAR_NAMES

if ! kubectl -n "$JDBC_SECRET_NAMESPACE" get secret -o name "$JDBC_SECRET_NAME"; then
  if ! kubectl -n "$JDBC_SECRET_NAMESPACE" create secret generic "$JDBC_SECRET_NAME" \
    --from-env-file="$JDBC_ENV_FN"; then
      echo "Malformed $JDBC_ENV_FN:" >&2
      cat $JDBC_ENV_FN >&2
      exit 1
  fi
fi
rm -f "$JDBC_ENV_FN"
unset   JDBC_ENV_FN

kubectl -n "$JDBC_SECRET_NAMESPACE" annotate secret "$JDBC_SECRET_NAME" \
  'replicator.v1.mittwald.de/replication-allowed=true' \
  'replicator.v1.mittwald.de/replication-allowed-namespaces=.*' \
  "replicator.v1.mittwald.de/replicate-to=$JDBC_NAMESPACE_CSV"


# we need this snowflake because it provisions the ELB
install_ingress
ingress_namespace='kube-system'

sniff_ingress_hostname() {
  kubectl -n "$ingress_namespace" get svc -o json -l app.kubernetes.io/instance=kong \
    | jq -r '.items[]
      | select(.spec.type=="LoadBalancer")
      | select(.status.loadBalancer)
      | .status.loadBalancer.ingress[0].hostname as $hn
      | select($hn)
      | $hn
    '
}
now=$(date +%s)
wait_seconds=600
poll_timeout=$(( now + wait_seconds ))
while [[ $(date +%s) -lt $poll_timeout ]]; do
  INGRESS_ELB=$(sniff_ingress_hostname || true)
  if [[ -n "$INGRESS_ELB" ]]; then
    # just because it is _allocated_ doesn't mean it's resolvable
    # DO NOT include --fail here since kong defaults to 404 route-not-found
    if curl -v --connect-timeout 1 "$INGRESS_ELB" 2>&1|grep -q '< HTTP'; then
      break
    else
      # pretend we didn't even see it, so the loop will retry normally
      INGRESS_ELB=''
    fi
  fi
  if [[ -n "$INGRESS_ELB" ]]; then
    export   INGRESS_ELB
    break
  fi
  sleep 5
done

if [[ -z "$INGRESS_ELB" ]]; then
  echo 'Waiting for Ingress ELB was unsuccessful' >&2
  exit 1
fi

if  kubectl -n kube-system get secret -o name helmfile0 >/dev/null 2>&1; then
    echo "Enabling ENG-2249 due to 'secret/helmfile0' existing" >&2
    # this will be true of retries, as well as stack conversions
    export SKIP_OUTCOME_EMAIL=1
    kubectl -n kube-system get secret -o json helmfile0 | \
        jq -r '.data["activation.json"]' | base64 --decode > "$ACTIVATION_PAYLOAD_FN"
fi

activate_cluster
# export the env-vars for helmfile
eval "$(_unpack_activation_payload)"

echo 'Sanity checking the "-o allexport"' >&2
# since printenv bombs if the env-var isn't exported
if [[ $(printenv COOKIE_SECRET | wc -c) == 0 ]] || [[ "$COOKIE_SECRET" == 'null' ]]; then
  echo 'Without COOKIE_SECRET the script will fail' >&2
  exit 1
fi

# we pass along tags from the CFN stack to the EBS CSI provider so it will tag the volumes
# said "tags_json" is expected to be in {"TheTag":"ItsValue"} "dict" format, which isn't the way .Tags is

CLUSTER_TAGS_JSON=$(jq -c '[ .Stacks[0].Tags[] | {key: .Key, value: .Value} ] | from_entries' "$cfn_json_fn")
export CLUSTER_TAGS_JSON


######################################
### Initial Helmfile Run
######################################

helmfile_verb=apply
report_outcome=failure
for _ in 1 2 3; do
  if helmfile --log-level=debug ${helmfile_verb} --concurrency 1; then
    zk_create_config_and_verify /app/v1/dmap/scheduling/maxConcurrent 5
    zk_create_config_and_verify /app/v1/dmap/scheduling/enabled false

    zk_create_config_and_verify /app/v1/s3/scheduling/enabled true
    zk_create_config_and_verify /app/v1/s3scan/maximumFileSizeToScan 1024

    zk_create_config_and_verify /upgrades/etag ""
    zk_create_config_and_verify /upgrades/payload ""

    zk_create_config_and_verify /app/v1/gsuite-plugin ""
    zk_create_config_and_verify /app/v1/office365-plugin ""
    zk_create_config_and_verify /app/v1/cloud-ingestion/aws "$AWS_REGION_CSV"
    report_outcome=success
    break
  fi
done

get_non_running_count() {
  # yes, it's possible to do this without jq
  kubectl get --all-namespaces=true -o json pods \
    | jq -r '
[
.items[]
| .status.phase as $p
| select($p != "Running" and $p != "Succeeded")
| $p
] | length
'
}

if [[ $report_outcome == success ]]; then
  # stall until all Pods known to us are Running
  ok=0
  for _ in $(seq 1 60); do
    non_running=$(get_non_running_count || echo "1")
    if [[ $non_running -eq 0 ]]; then
      # ensure things are stable
      ok=$(( ok + 1 ))
      if [[ $ok -eq 3 ]]; then
        break
      fi
    fi
    sleep 10
  done
fi

send_outcome_post $report_outcome
# since we have already sent the failure email, just give up
# (see the restart: OnFailure)
exit 0
