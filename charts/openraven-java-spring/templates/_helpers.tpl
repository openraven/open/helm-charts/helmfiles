{{/*
This differs from a normal chart where the chartname be valuable to know.
This, instead, only uses the .RelaseName since that is what we care about with this generic chart.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "openraven-java-spring.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "openraven-java-spring.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "openraven-java-spring.labels" -}}
helm.sh/chart: {{ include "openraven-java-spring.chart" . }}
{{ include "openraven-java-spring.selectorLabels" . }}
app.kubernetes.io/version: "{{ .Values.image.tag }}"
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "openraven-java-spring.selectorLabels" -}}
app.kubernetes.io/name: {{ include "openraven-java-spring.fullname" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "openraven-java-spring.serviceAccountName" -}}
{{- if (not (empty (or .Values.serviceAccount.create (and .Values.kiamAccountId .Values.kiamRole)))) }}
{{- default (include "openraven-java-spring.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Datadog Annotations Templates
*/}}

{{- define "openraven-java-spring.datadogIntegrationList" -}}
{{ if .Values.datadog.enabled }}
{{- $integrations := list }}
{{- range .Values.datadog.integrations }}
{{- if .enabled -}}
{{- $integrations = append $integrations .integration -}}
{{- end -}}
{{- end -}}
{{ $integrations | mustToJson }}
{{- end -}}
{{- end -}}

{{- define "openraven-java-spring.datadogInitConfigList" -}}
{{ if .Values.datadog.enabled }}
{{- $initConfigs := list }}
{{- range .Values.datadog.integrations }}
{{- if .enabled -}}
{{- $initConfigs = append $initConfigs .initConfig -}}
{{- end -}}
{{- end -}}
{{ $initConfigs | mustToJson }}
{{- end -}}
{{- end -}}

{{/* expects to be called with a LIST, the first item being the current context and the 2nd being $.Values.spring
since I cannot reach "upward" from within this define in order to try to resolve
any named references to their literal port number */}}
{{- define "openraven-java-spring.ddPromPort" -}}
{{- $selfPort := (index . 0).port }}
{{- $spring   := (index . 1) }}
{{- $portNumber := "" -}}
{{-  if int $selfPort -}}
{{-    $portNumber = $selfPort | toString -}}
{{-  else -}}
{{-    $portNumber = (get $spring $selfPort) | toString -}}
{{-  end -}}
{{ $portNumber }}
{{- end -}}

{{- define "openraven-java-spring.datadogInstancesList" -}}
{{- $instances := list }}
{{- range .Values.datadog.integrations }}
{{-   if .enabled -}}
{{-     $instanceConfig := dict -}}
{{-     $promPort := (include "openraven-java-spring.ddPromPort" (list . $.Values.spring)) -}}
{{/* be cautious, this uses 'print' on purpose, since printf would be percent-escape hell */}}
{{-     $prometheusUrl := print "http://%%host%%:" $promPort .path -}}
{{-     $_ := set $instanceConfig "prometheus_url" $prometheusUrl -}}
{{-     $_ = set $instanceConfig "namespace" $.Release.Name -}}
{{-     $_ = set $instanceConfig "metrics" .metrics -}}
{{-     $_ = set $instanceConfig "max_returned_metrics" .maxReturnedMetrics -}}
{{-     $_ = set $instanceConfig "exclude_metrics" .exclude_metrics -}}
{{-     $instances = append $instances $instanceConfig -}}
{{-   end -}}
{{- end -}}
{{ $instances | mustToPrettyJson }}
{{- end -}}

{{/* these annotations are 'define'-d as flush left, so the CALLER is responsible for indent-ing them */}}
{{- define "openraven-java-spring.datadogPodAnnotations" -}}
{{/* yes, we're squatting on this key to keep from updating everything in the world */}}
{{- with .Values.datadog.integrations.prometheus }}
{{-   if .enabled }}
prometheus.io/scrape: 'true'
prometheus.io/path: {{ .path }}
prometheus.io/port: {{ include "openraven-java-spring.ddPromPort" (list . $.Values.spring) | quote }}
{{-   end }}
{{- end }}
{{- with .Values.datadog.overrideAnnotations -}}
{{-   toYaml . -}}
{{- else }}
{{-   if .Values.datadog.enabled }}
ad.datadoghq.com/{{ $.Release.Name }}.check_names: |
{{- include "openraven-java-spring.datadogIntegrationList" . | nindent 2 }}
ad.datadoghq.com/{{ $.Release.Name }}.init_configs: |
{{- include "openraven-java-spring.datadogInitConfigList" . | nindent 2 }}
ad.datadoghq.com/{{ $.Release.Name }}.instances: |
{{- include "openraven-java-spring.datadogInstancesList" . | nindent 2 }}
{{-   end }}
{{- end }}
{{- end }}

{{- define "openraven-java-spring.kiamRoleName" -}}
orvn-{{ .Values.openraven.groupId }}-{{ .Values.kiamRole }}
{{- end }}
{{/*
Kiam Role Annotation Key:Value
*/}}
{{- define "openraven-java-spring.kiamRolePodAnnotation" -}}
{{- if .Values.kiamRole -}}
iam.amazonaws.com/role: {{ include "openraven-java-spring.kiamRoleName" . }}
{{- end }}
{{- end }}
