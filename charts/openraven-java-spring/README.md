# openraven-java-spring

A generic chart, tailored to Open Ravens typical Java Spring application

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| autoscaling.enabled | bool | `false` |  |
| autoscaling.maxReplicas | int | `100` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| autoscaling.targetMemoryUtilizationPercentage | string | `nil` |  |
| command | list | `[]` | Optionally supersede the container's command |
| databaseSchemaVersion | string | `nil` | to specify a databaseSchema for services that require it |
| datadog.enabled | bool | `true` |  |
| datadog.integrations | object | `{"prometheus":{"enabled":true,"initConfig":{},"integration":"openmetrics","maxReturnedMetrics":2000,"metrics":["*"],"path":"/actuator/prometheus","port":"serverManagementPort"}}` | enable or disables the all datadog autodiscover |
| datadog.integrations.prometheus | object | `{"enabled":true,"initConfig":{},"integration":"openmetrics","maxReturnedMetrics":2000,"metrics":["*"],"path":"/actuator/prometheus","port":"serverManagementPort"}` | map of integrations to configure &lt;integrationName>:            # (string) descriptive user defined value   enabled: true               # (true|false) whether to create this autodiscovery check   integration: "openmetrics"  # (string) the datadog integration name https://docs.datadoghq.com/agent/kubernetes/integrations/?tab=kubernetes   initConfig: {}              # (map) config values for integration https://docs.datadoghq.com/agent/kubernetes/integrations/?tab=kubernetes   maxReturnedMetrics: 2000    # (integer) the number of metrics and dimensions that can be returned. Used as a way to protect against cost overruns   metrics: ["*"]              # (list[string]) names of the metrics for the integration to collect   path: /metrics              # (string) the path of the check, absolute to localhost of the container   port: serverManagement      # (string) maybe either be the key to a value in the .Values.spring structure OR a quoted port number |
| datadog.overrideAnnotations | string | `nil` | If set, this map[string]string block is used for Datadog Annotations. When set it supersedes any other "integrations:" and implicitly sets "enabled: true" |
| envFrom | list | `[]` | List of Objects to pull into the environment like Secrets or ConfigMaps: # - secretRef: { name: "" } |
| externalIngress.annotations."konghq.com/https-redirect-status-code" | string | `"308"` |  |
| externalIngress.annotations."konghq.com/plugins" | string | `"customer-api-key-auth,customer-api-capture-1,customer-api-cors"` |  |
| externalIngress.annotations."konghq.com/protocols" | string | `"https"` |  |
| externalIngress.defaultPathType | string | `"ImplementationSpecific"` |  |
| externalIngress.enabled | bool | `false` | Use this value when path type is not specified, on each host:path |
| externalIngress.hosts[0].host | string | `nil` |  |
| externalIngress.hosts[0].paths[0].path | string | `nil` | if your path is a regex, kong now requires that it must start with `/~/` and the rest of your path https://docs.konghq.com/kubernetes-ingress-controller/2.7.x/guides/upgrade-kong-3x/#update-ingress-regular-expression-paths-for-kong-3x-compatibility |
| externalIngress.hosts[0].paths[0].pathType | string | `nil` |  |
| externalIngress.tls | list | `[]` |  |
| externalIngresses | list | `[]` |  |
| extraEnv | list | `[]` | List of Environment variables in a dictionary {"name": "<name>", "value": "<value>"} format |
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"Always"` |  |
| image.repository | string | `nil` |  |
| image.tag | string | `nil` |  |
| imagePullSecrets | list | `[]` |  |
| ingress.annotations."konghq.com/plugins" | string | `"oidc"` |  |
| ingress.className | string | `""` |  |
| ingress.defaultPathType | string | `"ImplementationSpecific"` |  |
| ingress.enabled | bool | `false` | Use this value when path type is not specified, on each host:path |
| ingress.hosts[0].host | string | `nil` |  |
| ingress.hosts[0].paths[0].path | string | `"/"` |  |
| ingress.hosts[0].paths[0].pathType | string | `nil` |  |
| ingress.tls | list | `[]` |  |
| ingresses | list | `[]` |  |
| initContainers | list | `[]` | List of init container specs. |
| java.vmArgs | string | `"-XX:+UseContainerSupport -XX:MaxRAMPercentage=75 -XX:InitialRAMPercentage=75 -Dlog4j2.formatMsgNoLookups=true"` |  |
| kiamAccountId | string | `nil` | the AWS Account ID in which the cluster is running, used to build a qualified IAM Role ARN, for use in EKS setups |
| kiamRole | string | `nil` | the common name of the role to be attached to the pod. It will be formatted in the annotation as `iam.amazonaws.com/role: orvn-<groupID>-<kiamRole>` |
| livenessProbe.httpGet.path | string | `"/actuator/health"` | path for the liveness probe |
| livenessProbe.httpGet.port | string | `"management"` | port for the liveness probe.  Defaults to {{ .Values.spring.serverManagementPort }} |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| openraven.groupId | string | `nil` | The GUID GroupId of the workspace. This is currently passed in from the Cluster-Upgrade Environment through Helmfiles |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| readinessProbe.httpGet.path | string | `"/actuator/health"` | path for the readiness probe |
| readinessProbe.httpGet.port | string | `"management"` | port for the readiness probe. Defaults to {{ .Values.spring.serverManagementPort }} |
| replicaCount | int | `1` |  |
| resources.limits.memory | string | `"2Gi"` |  |
| resources.requests.memory | string | `"2Gi"` |  |
| securityContext.allowPrivilegeEscalation | bool | `false` |  |
| securityContext.capabilities.add[0] | string | `"NET_BIND_SERVICE"` |  |
| securityContext.capabilities.drop[0] | string | `"ALL"` |  |
| sentry.dsn | string | `nil` |  |
| sentry.enabled | bool | `true` |  |
| server.maxHttpHeaderSize | string | `"128KB"` |  |
| service.enabled | bool | `false` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.create | bool | `false` |  |
| serviceAccount.name | string | `""` |  |
| spring.healthShowDetails | string | `"always"` |  |
| spring.profilesActive | string | `"default, prod"` |  |
| spring.serverManagementPort | string | `"8080"` |  |
| spring.serverPort | string | `"80"` |  |
| startupProbe | object | `{}` | if present, shaped just like its Probe friends above |
| tolerations | list | `[]` |  |
