{{/*
This differs from a normal chart where the chartname be valuable to know.
This, instead, only uses the .RelaseName since that is what we care about with this generic chart.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "openraven-common.fullname" -}}
{{- if .Values.fullnameOverride }}
{{-   .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{-   .Release.Name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "openraven-common.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "openraven-common.labels" -}}
helm.sh/chart: {{ include "openraven-common.chart" . }}
{{ include "openraven-common.selectorLabels" . }}
app.kubernetes.io/version: "{{ .Values.image.tag }}"
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "openraven-common.selectorLabels" -}}
app.kubernetes.io/name: {{ include "openraven-common.fullname" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "openraven-common.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{-   default (include "openraven-common.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{-   default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Datadog Annotations Templates
*/}}

{{- define "openraven-common.datadogIntegrationList" -}}
{{- $integrations := list }}
{{- range .Values.datadog.integrations }}
{{-   if .enabled -}}
{{-     $integrations = append $integrations .integration -}}
{{-   end -}}
{{- end -}}
{{ $integrations | mustToJson }}
{{- end -}}

{{- define "openraven-common.datadogInitConfigList" -}}
{{- $initConfigs := list }}
{{- range .Values.datadog.integrations }}
{{-   if .enabled -}}
{{-     $initConfigs = append $initConfigs .initConfig -}}
{{-   end -}}
{{- end -}}
{{ $initConfigs | mustToJson }}
{{- end -}}
{{/* expects to be called with a LIST, the first item being the current context and the 2nd being $.Values.spring
since I cannot reach "upward" from within this define in order to try to resolve
any named references to their literal port number */}}
{{- define "openraven-common.ddPromPort" -}}
{{- $selfPort := (index . 0).port }}
{{- $spring   := (index . 1) }}
{{- $portNumber := "" -}}
{{-  if int $selfPort -}}
{{-    $portNumber = $selfPort | toString -}}
{{-  else -}}
{{-    $portNumber = (get $spring $selfPort) | toString -}}
{{-  end -}}
{{ $portNumber }}
{{- end -}}

{{- define "openraven-common.datadogInstancesList" -}}
{{- $instances := list }}
{{- range .Values.datadog.integrations }}
{{-   if .enabled -}}
{{-     $instanceConfig := dict -}}
{{-     $promPort := (include "openraven-common.ddPromPort" (list . $.Values.spring)) -}}
{{/* be cautious, this uses 'print' on purpose, since printf would be percent-escape hell */}}
{{-     $prometheusUrl := print "http://%%host%%:" $promPort .path -}}
{{-     $_ := set $instanceConfig "prometheus_url" $prometheusUrl -}}
{{-     $_ = set $instanceConfig "namespace" $.Release.Name -}}
{{-     $_ = set $instanceConfig "metrics" .metrics -}}
{{-     $_ = set $instanceConfig "max_returned_metrics" .maxReturnedMetrics -}}
{{-     $_ = set $instanceConfig "exclude_metrics" .exclude_metrics -}}
{{-     $instances = append $instances $instanceConfig -}}
{{-   end -}}
{{- end -}}
{{ $instances | mustToPrettyJson }}
{{- end -}}

{{- define "openraven-common.datadogPodAnnotations" -}}
{{- with .Values.datadog.integrations.prometheus }}
{{-   if .enabled }}
prometheus.io/scrape: 'true'
prometheus.io/path: {{ .path }}
prometheus.io/port: {{ include "openraven-common.ddPromPort" (list . $.Values.spring) | quote }}
{{  end }}
{{- end }}
{{- if .Values.datadog.overrideAnnotations -}}
{{- .Values.datadog.overrideAnnotations -}}
{{- else if .Values.datadog.enabled -}}
ad.datadoghq.com/{{ .Release.Name }}.check_names: |
{{- include "openraven-common.datadogIntegrationList" . | nindent 2 }}
ad.datadoghq.com/{{ .Release.Name }}.init_configs: |
{{- include "openraven-common.datadogInitConfigList" . | nindent 2 }}
ad.datadoghq.com/{{ .Release.Name }}.instances: |
{{- include "openraven-common.datadogInstancesList" . | nindent 2 }}
{{- end }}
{{- end }}

{{- define "openraven-common.kiamRoleName" -}}
orvn-{{ .Values.openraven.groupId }}-{{ .Values.kiamRole }}
{{- end }}
{{/*
Kiam Role Annotation
*/}}
{{- define "openraven-common.kiamRolePodAnnotation" -}}
{{- if .Values.kiamRole -}}
iam.amazonaws.com/role: orvn-{{ .Values.openraven.groupId }}-{{ .Values.kiamRole }}
{{- end }}
{{- end }}

{{/*
Probe
*/}}
{{- define "openraven-common.probe" -}}
{{- $default := index . 0 -}}
{{- $probe := index . 1 -}}
httpGet:
  path: {{ $probe.path | default $default.path }}
  port: {{ $probe.port | default $default.port }}
{{- $settings := $probe.settings }}
{{- if $probe.mergeSettings | default $default.mergeSettings  }}
{{-   $settings = mustMerge $settings $default.settings }}
{{- end }}
{{- if $settings }}
{{    $settings | toYaml  }}
{{- end }}
{{- end }}
