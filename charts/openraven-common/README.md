# openraven-common

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.16.0](https://img.shields.io/badge/AppVersion-1.16.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| autoscaling.enabled | bool | `false` |  |
| autoscaling.maxReplicas | int | `100` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| command | list | `[]` | Optionally supersede the container's command |
| cronJob.enabled | bool | `false` | Here as a stub for future work. If you expect it to work out of the box, you're gonna have a bad time. |
| cronJob.schedule | string | `nil` |  |
| datadog.enabled | bool | `true` | enable or disables the all datadog autodiscover |
| datadog.integrations | object | `{}` | map of integrations to configure |
| datadog.overrideAnnotations | string | `nil` | If set, this map[string]string block is used for Datadog Annotations. When set it supersedes any other "integrations:" and implicitly sets "enabled: true" |
| deployment.enabled | bool | `true` |  |
| envFrom | list | `[]` | List of Objects to pull into the environment like Secrets or ConfigMaps: # - secretRef: { name: "" } |
| extraEnv | object | `{}` | Dictionary of Key: Value Environment variables in a dictionary {"KEY": "VALUE"} format |
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `nil` |  |
| image.tag | string | `nil` |  |
| imagePullSecrets | list | `[]` |  |
| ingress.annotations | object | `{}` |  |
| ingress.className | string | `""` |  |
| ingress.defaultPathType | string | `"ImplementationSpecific"` |  |
| ingress.defaultPortName | string | `"http"` |  |
| ingress.enabled | bool | `false` | Use this value when path type is not specified, on each host:path |
| ingress.hosts[0].host | string | `nil` |  |
| ingress.hosts[0].paths[0].path | string | `"/"` |  |
| ingress.hosts[0].paths[0].pathType | string | `nil` |  |
| ingress.hosts[0].paths[0].portName | string | `nil` |  |
| ingress.tls | list | `[]` |  |
| ingresses | list | `[]` | a list of ingress objects with the same format at `ingress` |
| initContainers | list | `[]` | List of init container specs. |
| kiamRole | string | `nil` | the common name of the role to be attached to the pod. It will be formatted in the annotation as `iam.amazonaws.com/role: orvn-<groupID>-<kiamRole>` |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| openraven.groupId | string | `nil` | The Okta GroupId of the workspace. This is currently passed in from the Cluster-Upgrade Environment through Helmfiles |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| ports | list | `[{"containerPort":80,"name":"http","protocol":"TCP"}]` | List of ports to expose on the pod |
| probes.default.mergeSettings | bool | `true` | When true, probes.default.settings will be _merged_ with $specificProbe.settings. When false, the the $specificProbe.settings will be used without merging |
| probes.default.path | string | `"/"` |  |
| probes.default.port | string | `"http"` |  |
| probes.default.settings | object | `{}` | a random map[] that will be appended under the httpGet, if specified |
| probes.livenessProbe.enabled | bool | `true` |  |
| probes.livenessProbe.mergeSettings | string | `nil` |  |
| probes.livenessProbe.path | string | `nil` |  |
| probes.livenessProbe.port | string | `nil` |  |
| probes.livenessProbe.settings | object | `{}` |  |
| probes.readinessProbe.enabled | bool | `false` |  |
| probes.readinessProbe.mergeSettings | string | `nil` |  |
| probes.readinessProbe.path | string | `nil` |  |
| probes.readinessProbe.port | string | `nil` |  |
| probes.readinessProbe.settings | object | `{}` |  |
| probes.startupProbe.enabled | bool | `false` |  |
| probes.startupProbe.mergeSettings | string | `nil` |  |
| probes.startupProbe.path | string | `nil` |  |
| probes.startupProbe.port | string | `nil` |  |
| probes.startupProbe.settings | object | `{}` |  |
| replicaCount | int | `1` |  |
| resources | object | `{}` |  |
| securityContext | object | `{}` |  |
| service.ports | list | `[{"name":"http","port":80,"protocol":"TCP","targetPort":"http"}]` | List of ports to use for the service |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `false` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| tolerations | list | `[]` |  |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.5.0](https://github.com/norwoodj/helm-docs/releases/v1.5.0)
