{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "aws-discovery-svc.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "aws-discovery-svc.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "aws-discovery-svc.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "aws-discovery-svc.labels" -}}
app.kubernetes.io/name: {{ include "aws-discovery-svc.name" . }}
helm.sh/chart: {{ include "aws-discovery-svc.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{- define "aws-discovery-svc.wantServiceAccount" -}}
{{- .Values.serviceAccount.create  -}}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "aws-discovery-svc.serviceAccountName" -}}
{{- if (include "aws-discovery-svc.wantServiceAccount" .) }}
{{-   default (include "aws-discovery-svc.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
{{-   default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{- define "aws-discovery-svc.priorityClassName" -}}
{{ .Values.priorityClass.baseName }}-{{ .Values.priorityClass.priority }}
{{- end -}}

{{- define "aws-discovery-svc.kiamRoleName" -}}
orvn-{{ .Values.global.groupId }}-{{ .Values.kiamRole }}
{{- end }}
{{/*
Kiam Role Annotation Key:Value
*/}}
{{- define "aws-discovery-svc.kiamRolePodAnnotation" -}}
{{- if .Values.kiamRole -}}
iam.amazonaws.com/role: {{ include "aws-discovery-svc.kiamRoleName" . }}
{{- end }}
{{- end }}

{{/*
Datadog Annotations Templates
*/}}

{{- define "aws-discovery-svc.datadogIntegrationList" -}}
{{ if .Values.datadog.enabled }}
{{- $integrations := list }}
{{- range .Values.datadog.integrations }}
{{- if .enabled -}}
{{- $integrations = append $integrations .integration -}}
{{- end -}}
{{- end -}}
{{ $integrations | mustToJson }}
{{- end -}}
{{- end -}}

{{- define "aws-discovery-svc.datadogInitConfigList" -}}
{{ if .Values.datadog.enabled }}
{{- $initConfigs := list }}
{{- range .Values.datadog.integrations }}
{{- if .enabled -}}
{{- $initConfigs = append $initConfigs .initConfig -}}
{{- end -}}
{{- end -}}
{{ $initConfigs | mustToJson }}
{{- end -}}
{{- end -}}

{{- define "aws-discovery-svc.datadogInstancesList" -}}
{{- $instances := list }}
{{- range .Values.datadog.integrations }}
{{- if .enabled -}}
{{- $instanceConfig := dict -}}
{{- $portNumber := "" -}}
{{- if  int .port -}}
{{- $portNumber = .port | toString -}}
{{- else -}}
{{- $portNumber = (get $.Values.spring .port) | toString -}}
{{- end -}}
{{- $prometheusUrl := print "http://%%host%%:" $portNumber .path -}}
{{- $_ := set $instanceConfig "prometheus_url" $prometheusUrl -}}
{{- $_ = set $instanceConfig "namespace" $.Release.Name -}}
{{- $_ = set $instanceConfig "metrics" .metrics -}}
{{- $instances = append $instances $instanceConfig -}}
{{- end -}}
{{- end -}}
{{ $instances | mustToPrettyJson }}
{{- end -}}

{{- define "aws-discovery-svc.datadogPodAnnotations" -}}
{{- if .Values.datadog.overrideAnnotations -}}
{{- .Values.datadog.overrideAnnotations -}}
{{- else if .Values.datadog.enabled -}}
ad.datadoghq.com/{{ .Release.Name }}.check_names: |
{{- include "aws-discovery-svc.datadogIntegrationList" . | nindent 2 }}
ad.datadoghq.com/{{ .Release.Name }}.init_configs: |
{{- include "aws-discovery-svc.datadogInitConfigList" . | nindent 2 }}
ad.datadoghq.com/{{ .Release.Name }}.instances: |
{{- include "aws-discovery-svc.datadogInstancesList" . | nindent 2 }}
{{- end }}
{{- end }}
