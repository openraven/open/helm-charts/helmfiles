#!/usr/bin/env bash
set -euo pipefail

BRANCH_NAME="release-4.0"
echo "Downloading crds"
cd crds || exit 1
curl -fsSLO https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/${BRANCH_NAME}/client/config/crd/snapshot.storage.k8s.io_volumesnapshotclasses.yaml
curl -fsSLO https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/${BRANCH_NAME}/client/config/crd/snapshot.storage.k8s.io_volumesnapshotcontents.yaml
curl -fsSLO https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/${BRANCH_NAME}/client/config/crd/snapshot.storage.k8s.io_volumesnapshots.yaml
cd ..

echo "Downloading templates"
cd templates || exit 1
curl -fsSLO https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/${BRANCH_NAME}/deploy/kubernetes/snapshot-controller/rbac-snapshot-controller.yaml
curl -fsSLO https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/${BRANCH_NAME}/deploy/kubernetes/snapshot-controller/setup-snapshot-controller.yaml

echo "Please fix namespace references in the new templates"
