/**
 * This script looks for .md files in a directory recursively
 * and attempts to upsert them to the README project with
 * apiKey.
 *
 * Files are updated based off of a slug. Slugs are auto-generated, but can also be
 * manually specified in the "matter" block at the top of your markdown block.
 */

const sdk = require('api')('@developers/v2.0#nysezql0wwo236');
const yargs = require('yargs');
const {resolve, extname, basename} = require('path');
const {readdir} = require('fs').promises;
// The same library README IO uses for their CLI tool https://docs.readme.com/docs/rdme#markdown-file-setup
const matter = require('gray-matter');

const argv = yargs
  .option('apiKey', {
    description: 'readme io API key',
    alias: 'k',
    type: 'string',
    demandOption: true
  })
  .option('directory', {
    description: 'directory holding Markdown files with .md extension name',
    alias: 'd',
    type: 'string',
    demandOption: true
  })
  .option(
    'readmeVersion', {
      description: 'version of readme to push to. Must already exist on Readme io site',
      alias: 'r',
      type: 'string',
      demandOption: true
    }
  )
  .help()
  .alias('help', 'h').argv;

sdk.auth(argv.apiKey);
const genericPageAll = async (getPage) => {
  let page = 1;
  let hasMore = true;
  const allData = [];
  while (hasMore) {
    const data = await getPage(page);
    allData.push(...data);
    page++;
    hasMore = data != null && data.length > 0;
  }
  return allData;
};
const getAllCategories = async () => genericPageAll(async (page = 1) => sdk.getCategories(
  {
    perPage: '10',
    page: page,
    // Note that whenever a Readme version is forked all Open API specs get new ID's!
    // Using version ensures that we are updating the proper document in the right version
    'x-readme-version': argv.readmeVersion
  })
);

/**
 * Get all filePaths in dir recursively
 * @param dir to get filePaths
 * @returns {Promise<unknown[]>}
 */
async function getFiles(dir) {
  const dirents = await readdir(dir, {withFileTypes: true});
  const files = await Promise.all(dirents.map((dirent) => {
    const res = resolve(dir, dirent.name);
    return dirent.isDirectory() ? getFiles(res) : res;
  }));
  return Array.prototype.concat(...files);
}

/**
 * Upsert markdown doc
 */
async function upsertDoc({
                           data: {
                             title,
                             slug,
                             category,
                             hidden,
                           },
                           content,
                         }) {
  let docExists;
  try {
    await sdk.getDoc({
      slug,
      'x-readme-version': argv.readmeVersion
    });
    docExists = true;
  } catch (e) {
    console.debug(
      `Markdown Doc with slug ${slug} does not already exist. We will insert instead of updating. This error is normal and does not need to be actions`,
      e
    );
    docExists = false;
  }
  if (docExists) {
    try {
      await sdk.updateDoc(
        {
          hidden,
          title,
          category,
          body: content,
        },
        {
          slug,
          'x-readme-version': argv.readmeVersion,
        }
      );
      console.log(`Successfully updated doc with title ${title}, slug ${slug}`);
    } catch (e) {
      const err = `Could not update doc with title ${title}, slug ${slug}`;
      console.error(err, e);
      throw new Error(e);
    }
  } else {
    try {
      await sdk.createDoc(
        {
          hidden,
          title,
          category,
          body: content,
          slug
        },
        {
          'x-readme-version': argv.readmeVersion,
        }
      );
      console.log(`Successfully created doc with title ${title}, slug ${slug}`);
    } catch (e) {
      const err = `Could not create doc with title ${title}, slug ${slug}`;
      console.error(err, e);
      throw new Error(e);
    }
  }
}

async function main() {
  console.log(`Attempting to update readme markdown files from directory ${argv.directory}`);
  const categories = await getAllCategories();
  const defaultCategory = categories.find(x => x.slug === 'documentation');
  if (!defaultCategory) {
    const err = `Readme Docs require a category internally to be uploaded. By default we expect the default category with slug 'documentation' to exist
       This must be manually fixed. Go create that category in the project site.`;
    console.error(err);
    throw new Error(err);
  }

  const filePaths = await getFiles(argv.directory);
  const markdownFiles = filePaths
    .filter(x => extname(x) === '.md');
  for (let filePathsKey of markdownFiles) {
    // Get data properties from matter block.
    const {
      data: {
        title,
        slug,
        category,
        hidden = false,
      },
      content,
    } = matter.read(filePathsKey);
    if (!title) {
      const err = `MD file ${filePathsKey} does not have a title in front matter. See example to fix: https://docs.readme.com/docs/rdme#markdown-file-setup`;
      console.error(err);
      throw new Error(err);
    }
    // If slug is not specified, auto generate one based on the file name
    const finalSlug = slug || basename(filePathsKey).replace(extname(filePathsKey), '').toLowerCase();

    // By convention, we match the String in category with the "slug" of an existing category.
    // If this doesn't exist your doc will be uploaded to the default category
    let finalCategoryId = defaultCategory.id;
    if (category) {
      const designatedCategory = categories.find(x => x.slug.trim() === category.trim());
      if (designatedCategory) {
        finalCategoryId = designatedCategory.id;
      } else {
        console.info(`Doc with filePath '${filePathsKey}' had category defined as '${category}'.
        We could not find an existing category with this slug. The default category "Documentation" will be used instead.
        If this is a problem, manually fixing it will involve you fixing the category header in your CI Generated Doc
        and removing the document created in the default Documentation category.
        `);
      }
    }
    await upsertDoc({
      data: {
        title,
        slug: finalSlug,
        category: finalCategoryId,
        hidden,
      },
      content,
    })
  }
}

main().catch(() => {
  process.exitCode = 1;
});
