# Readme Publish

Our Readme IO Publish CLI tools stage and publish two sets of API docs:
1. [Internal](https://developer-internal.openraven.com/docs) - an API doc for internal usage by Open Raven devs
2. [External](https://developer.openraven.com/docs) - API docs that are publicly available for usage by customers

There are three main entry point scripts:

1. ./download-readme-artifacts.sh . In this script, we download files from an S3 Bucket hosting our API Doc artifacts.
This is done per service and "versioning" of this is done by using the Image Tag of a particular service. This makes sure
that the right set of docs are published for the Docker Image Tags of our services.
These files are downloaded and formatted in directories for consumption by subsequent steps.
2. ./stage-api-docs.sh . In this script, Open API and markdown docs downloaded from the previous step are upserted to a "staging version"
using a Readme IO API keys. This is done for both the Internal and External doc sites, by using their API keys provided by CI variables.
3. ./publish-api-docs.sh . In this script we publish our staging docs. We accomplish by doing an "in place upgrade".
By the end of the script, we'll have a new "prod" version that is a copy of our "staging" version.
Internally, this is done by forking "staging" into a new version called "staged" and making that the [Main version](https://docs.readme.com/docs/versions#main-version)
Afterwards, we delete "prod" and rename "staged" to "prod". Thus, the "in place upgrade" is completed.
Deleting versions is important because Readme IO limits the number of versions you can have. 

## How do I add my Services Docs?

In ./download-readme-artifacts.sh at the bottom is a list of services that we get artifacts for. Add your service
there making sure to reference the image file and the directory your services artifacts are located in S3.

## More details about ./stage-api-docs.sh

The Open API artifacts you upload must be `.json` files. These json files are expected to have unique
Open API titles (e.g. info.title in json object) because titles are used as the ID for upserting your
Open API definition.

Markdown artifacts are supported mainly for the usecase of GraphQL Schemas, but other `.md` files can be
uploaded too. Note that files must use the `.md` extension, other variants are not accepted.

Markdown artifacts parsing uses `matter`, a JS library that allows you to put details into a formatted
header at the top of the file. This follows Readme IO's own [tooling standards](https://docs.readme.com/docs/rdme#specifying-page-slugs).

We support the following matter properties:
* title - title of the page
* slug - slug used in URL. THIS IS effectively the id of the document. Please make sure slugs are unique, otherwise things may be overwritten
         Note that this is optional, when not supplied we auto generate a slug based off the markdown fileName
* category - category slug, please use the slug as the ID here. If not supplied, then this file will be placed in a default category.
* hidden - whether the doc is hidden or not. Chances are you want this to be false.

The top of your `.md` file might look like:
```markdown
---
title: My GraphQL Schema
slug: my-graphql-schema
category: graphql-apis
---

... your content
```
Note that the matter within the '---' block will be stripped away before upload! This is there to help
with configuration.
