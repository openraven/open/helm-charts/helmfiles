#! /usr/bin/env bash

set -euo pipefail
echo "Downloading artifacts from S3"
API_DOC_BUCKET="$ARTIFACTS_API_DOC_BUCKET"
mkdir -p s3dl

mkdir -p api-artifact-extraction/external
mkdir -p api-artifact-extraction/internal
mkdir -p api-docs/external
mkdir -p api-docs/internal

helmfilesAlternateRegex='"?[0-9]+\.([0-9]*)\.[0-9]+"?'
removeSurroundingQuotesRegex='"?([0-9]+)"?'

API_DOC_TAR_LINKS=()

# Extract image tag from yaml file
getImageTag () {
  local filePath=$1
  local tagStr=$(grep -A3 'tag:' $filePath | tail -n1 | awk '{print $2}')
   if [[ "$tagStr" =~ $helmfilesAlternateRegex ]]
   then
     tag="${BASH_REMATCH[1]}"
     echo $tag
   else
     if [[ "$tagStr" =~ $removeSurroundingQuotesRegex ]]
     then
       tag="${BASH_REMATCH[1]}"
       echo $tag
     else
       echo "no matches found for image tag $tagStr" >&2
     fi
   fi
}

extractFiles () {
  # Path of API Doc tar file
  local tarFilePath=$1;
  # Path to extract to tar file to. This is an intermediate step
  local artifactExtractPath=$2
  # After artifact extraction, we will look for .md and .json files in
  # artifactExtractPath. Any matching files will be moved to the final outputDir
  local outputDir=$3

  if [ -f $tarFilePath ]
  then
    echo "Found ${tarFilePath} extracting any markdown and openapi.json files from it"
    tar -xvzf $tarFilePath -C $artifactExtractPath
    mkdir -p $outputDir/markdown
    mkdir -p $outputDir/openapi
    # NOTE this flattens the file paths, which makes name collisions iffy. However, if users are using the upstream API doc tooling
    # name collisions should not occur here.
    find $artifactExtractPath -name "*.md" -exec mv '{}' $outputDir/markdown \;
    find $artifactExtractPath -name "*.json" -exec mv '{}' $outputDir/openapi \;
  fi
}

# Download from API_DOC_BUCKET for a specific directory (per service) and image tag.
downloadFromAws () {
  local directoryName=$1;
  local tag=$2;

  local awsDownloadFolder=s3dl/$directoryName
  mkdir -p $awsDownloadFolder
  aws s3 cp s3://$API_DOC_BUCKET/api-docs/$directoryName/$tag $awsDownloadFolder --recursive

  local s3LinkPrefix="https://$API_DOC_BUCKET.s3.amazonaws.com/api-docs/$directoryName/$tag";
  local internalTar="internal.tar.gz"
  local externalTar="external.tar.gz"
  # Add tar links for Markdown file containing API links
  API_DOC_TAR_LINKS+=("- [$directoryName Internal API]($s3LinkPrefix/$internalTar)")
  API_DOC_TAR_LINKS+=("- [$directoryName External API]($s3LinkPrefix/$externalTar)")

  mkdir -p api-artifact-extraction/internal/$directoryName
  extractFiles $awsDownloadFolder/$internalTar api-artifact-extraction/internal/$directoryName api-docs/internal/$directoryName
  mkdir -p api-artifact-extraction/external/$directoryName
  extractFiles $awsDownloadFolder/$externalTar api-artifact-extraction/external/$directoryName api-docs/external/$directoryName
}

getArtifacts () {
  local filePath=$1
  local s3DirectoryName=$2
  local tag=$(getImageTag $filePath)
  downloadFromAws $s3DirectoryName $tag
}

# Download artifacts for micro services
for i in credential-manager data-catalog sdss aws-security-group-meta-api access-analysis scan-service asset-groups; do
  getArtifacts helmfile.d/values/${i}-image.yaml $i
done

if (( ${#API_DOC_TAR_LINKS[@]} > 0 ))
then
  # Create a delineation
  echo ----------
  echo "Creating markdown API Doc with download links for internal docs site:"
  {
    cat .gitlab/readme-publish/api-doc-downloads.md
    echo
    for i in "${API_DOC_TAR_LINKS[@]}"
    do
      echo "$i"
    done
  } > api-docs/internal/api-doc-downloads.md
fi
