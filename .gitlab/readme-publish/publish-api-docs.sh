#! /usr/bin/env bash

set -euo pipefail

# Node Scripts
cd "$(dirname "$0")"
npm install
echo "PUBLISHING EXTERNAL API DOCS"
node ./publish-version -p $EXTERNAL_API_DOCS_PROD_VERSION -s $EXTERNAL_API_DOCS_STAGING_VERSION -k $README_IO_EXTERNAL_API_KEY

echo "PUBLISHING INTERNAL API DOCS"
node ./publish-version -p $INTERNAL_API_DOCS_PROD_VERSION -s $INTERNAL_API_DOCS_STAGING_VERSION -k $README_IO_INTERNAL_API_KEY
