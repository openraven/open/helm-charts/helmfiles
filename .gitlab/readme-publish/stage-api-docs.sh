#! /usr/bin/env bash

set -euo pipefail
GITLAB_WORKING_DIRECTORY="$CI_PROJECT_DIR"
echo "Staging API docs"

# Node Scripts
cd "$(dirname "$0")"
npm install

# change to use dir and recursively look for .json files
# use yargs
echo "STAGING EXTERNAL OPEN API"
node ./update-open-api.js -k $README_IO_EXTERNAL_API_KEY -r $EXTERNAL_API_DOCS_STAGING_VERSION -d $GITLAB_WORKING_DIRECTORY/api-docs/external
node ./update-readme-markdown.js -k $README_IO_EXTERNAL_API_KEY -r $EXTERNAL_API_DOCS_STAGING_VERSION -d $GITLAB_WORKING_DIRECTORY/api-docs/external

echo "STAGING INTERNAL OPEN API"
node ./update-open-api.js -k $README_IO_INTERNAL_API_KEY -r $INTERNAL_API_DOCS_STAGING_VERSION -d $GITLAB_WORKING_DIRECTORY/api-docs/internal
node ./update-readme-markdown.js -k $README_IO_INTERNAL_API_KEY -r $INTERNAL_API_DOCS_STAGING_VERSION -d $GITLAB_WORKING_DIRECTORY/api-docs/internal
