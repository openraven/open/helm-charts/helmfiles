---
title: "API Downloads for Postman, Insomnia, etc."
slug: open-api-downloads
---

Here you'll find links to tar files containing Open API definitions for our different services. These are helpful for folks who prefer API tools, like Postman and Insomnia, that support Open API imports.

However, please take note of a couple of things:
* Readme IO automatically updates with the latest versions of API docs. Your local Postman will not, so you will need to redownload definitions whenever they are changed.
* Our definitions are always optimized for Readme IO usage. While we welcome feedback on the Postman experience, there will be cases where nothing can be done about it.

# Download Links
