const sdk = require('api')('@developers/v2.0#nysezql0wwo236');
const yargs = require('yargs');
/**
 * This node script promotes a stagingVersion to prodVersion. The overall process is that we fork a new version called
 * `${prodVersion}-staged` off of stagingVersion. This new "staged" version becomes the "Readme IO main version", the version
 * that shows up by default for users.
 * After this is complete, we delete prodVersion and rename `${prodVersion}-staged` to prodVersion. The replacement is then
 * complete.
 *
 * NOTE: it is important prior to this script that ${prodVersion}-staged DOES NOT EXIST in Readme IO. Otherwise, the fork
 * and version create will fail
 */
const argv = yargs
  .option('prodVersion', {
    description: 'Prod version will be removed and replaced with staging version. Must already exist in the Readme Site',
    alias: 'p',
    type: 'string',
    demandOption: true
  })
  .option('stagingVersion', {
    description: 'Staging version to fork from. Must already exist in the Readme Site',
    alias: 's',
    type: 'string',
    demandOption: true
  })
  .option('apiKey', {
    description: 'readme io API key',
    alias: 'k',
    type: 'string',
    demandOption: true
  })
  .help()
  .alias('help', 'h').argv;

sdk.auth(argv.apiKey);

async function forkAndCreateNewMainVersion() {
  const stagedVersionString = `${argv.prodVersion}-staged`;
  try {
    await sdk.createVersion({
      is_beta: false,
      version: stagedVersionString,
      from: argv.stagingVersion,
      is_stable: true,
    });
    console.log(`Created ${stagedVersionString} forked from ${argv.stagingVersion} as the main version.`);
  } catch (e) {
    console.error(`Error creating ${stagedVersionString}`, e);
    throw e;
  }

  try {
    await sdk.deleteVersion({versionId: argv.prodVersion});
    console.log(`Deleted previous main version ${argv.prodVersion}`);
  } catch (e) {
    console.error(
      `Error deleting previous main version ${argv.prodVersion}
             To Manually fix, delete previous main version in UI and rename ${stagedVersionString} to
             proper production version e.g. ${argv.prodVersion}.`,
      e
    );
    throw e;
  }
  try {
    await sdk.updateVersion({
      is_beta: false,
      version: argv.prodVersion,
      from: argv.stagingVersion,
      is_stable: true,
      is_deprecated: false
    }, {versionId: stagedVersionString});
    console.log(`Renamed ${stagedVersionString} to ${argv.prodVersion}. Publish complete!`);
  } catch (e) {
    console.error(
      `Failed to rename ${stagedVersionString} to ${argv.prodVersion}.
             To manually remediate, go to UI and rename ${stagedVersionString} to proper production version
             e.g. ${argv.prodVersion}`,
      e
    );
    throw e;
  }
}

async function main() {
  await forkAndCreateNewMainVersion();
}

main().catch(() => {
  process.exitCode = 1;
});
