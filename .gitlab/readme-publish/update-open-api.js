/**
 * This script looks for json files in a directory recursively
 * and attempts to upsert them to the README project with
 * apiKey.
 * Updates are done by keying off of the title key within the Open API json file.
 * If the title of an Open API json changes, the old docs will need to be manually deleted.
 */

const sdk = require('api')('@developers/v2.0#nysezql0wwo236');
const yargs = require('yargs');
const fs = require('fs');
const {resolve, extname} = require('path');
const {readdir} = require('fs').promises;

const argv = yargs
  .option('apiKey', {
    description: 'readme io API key',
    alias: 'k',
    type: 'string',
    demandOption: true
  })
  .option('directory', {
    description: 'directory holding Open API files with .json extension name',
    alias: 'd',
    type: 'string',
    demandOption: true
  })
  .option(
    'readmeVersion', {
      description: 'version of readme to push to. Must already exist on Readme io site',
      alias: 'r',
      type: 'string',
      demandOption: true
    }
  )
  .help()
  .alias('help', 'h').argv;

sdk.auth(argv.apiKey);
const pageThroughOpenApiSpec = (page = 1) => {
  return sdk.getAPISpecification(
    {
      perPage: '10',
      page: page,
      // Note that whenever a Readme version is forked all Open API specs get new ID's!
      // Using version ensures that we are updating the proper document in the right version
      'x-readme-version': argv.readmeVersion
    }
  );
};

/**
 * Gets all Open API specs from Readme IO API
 * @returns {Promise<*[]>} Readme IO Open API specs
 */
const getAllOpenApiSpecs = async () => {
  let page = 1;
  let hasMore = true;
  const allData = [];
  while (hasMore) {
    const data = await pageThroughOpenApiSpec(page);
    allData.push(...data);
    page++;
    hasMore = data != null && data.length > 0;
  }
  return allData;
}

/**
 * Recursively get all filePaths in dir
 * @param dir to look for files in
 * @returns {Promise<String[]>} file paths
 */
async function getFiles(dir) {
  const dirents = await readdir(dir, {withFileTypes: true});
  const files = await Promise.all(dirents.map((dirent) => {
    const res = resolve(dir, dirent.name);
    return dirent.isDirectory() ? getFiles(res) : res;
  }));
  return Array.prototype.concat(...files);
}

/**
 * @param filePath path to file to extract title
 * @returns {Promise<*>} resolves to a String representing the title of the Open API definition
 */
async function findTitleFromFile(filePath) {
  const openAPIObj = JSON.parse(fs.readFileSync(filePath, 'utf8'));
  const {info = {}} = openAPIObj;
  if (!info.title) {
    throw new Error(`${filePath} does not contain a valid Open API title`);
  }
  return info.title;
}

/**
 * Gets a map of titles to filePaths
 * @param dir to find potential Open API filePths in
 * @returns {Promise<{}>}
 */
async function getFileMap(dir) {
  const files = await getFiles(dir);
  const filteredFiles = files.filter(x => extname(x) === '.json');
  const acc = {};
  for (const filePath of filteredFiles) {
    const title = await findTitleFromFile(filePath);
    if (acc[title]) {
      const message = `Encountered an Open API file with duplicate name called ${title}. This script treats titles as unique, this must be fixed manually`;
      console.error(message);
      throw new Error(message);
    }
    acc[title] = filePath;
  }
  return acc;
}

async function main() {
  const specMap = await getFileMap(argv.directory);
  if (Object.keys(specMap).length === 0) {
    console.info(`No Open API json files are in ${argv.directory}. Nothing will be published`);
    return;
  }

  let existingSpecs;
  try {
    existingSpecs = await getAllOpenApiSpecs();
  } catch (err) {
    console.error("Failed to get existing Open API Specs", err);
    throw err;
  }
  const existingSpecMap = existingSpecs.reduce((acc, spec) => {
    if (acc[spec.title]) {
      const message = `Encountered a Open API file with duplicate name called ${spec.title}. This script treats names as unique, this must be fixed manually`;
      console.error(message);
      throw new Error(message);
    }
    acc[spec.title] = spec;
    return acc;
  }, {});

  for (const [title, filePath] of Object.entries(specMap)) {
    // If this is an existing Open API spec, update
    if (existingSpecMap[title]) {
      const id = existingSpecMap[title].id;
      try {
        const res = await sdk.updateAPISpecification(
          {spec: filePath},
          {id: id}
        );
        console.info(`Updated spec with title ${title} and filePath ${filePath}. Response ${JSON.stringify(res)}`);
      } catch (err) {
        console.error(`Failed to update spec with title ${title} and filePath ${filePath} and id ${id}`, err);
        throw err;
      }
    } else {
      // New Open API Spec, insert
      try {
        const res = await sdk.uploadAPISpecification(
          {spec: filePath},
          {'x-readme-version': argv.readmeVersion}
        );
        console.info(`Posted new spec with title ${title} and filePath ${filePath}. Response ${JSON.stringify(res)}`);
      } catch (err) {
        console.error(`Failed to create spec with title ${title} and filePath ${filePath}`, err);
        throw err;
      }
    }
  }
}

main().catch(() => {
  process.exitCode = 1;
});
