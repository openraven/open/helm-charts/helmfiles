#! /usr/bin/env bash
set -euo pipefail
CI="${CI:-}"
CHATOPS="${CHAT_CHANNEL:-}"
TRACE="${TRACE:-}"
trace_on() {
    if [[ -n "$TRACE" ]]; then
      set -x
    fi
}
trace_on
if [[ $# -le 2 ]]; then
    cat >&2 <<USAGE
Usage: $0 values-file-path chart-name patch-version [major-minor-pair]
 values-file-path == the path to a per-chart "-version.yaml" that is relative to project root
 chart-name == the basename of charts/* to which the bump is happening (e.g. account-management)
 patch-version == the incremental bump that will be updated to the existing major.minor found in the file
 major-minor-pair == OPTIONAL explicitly provided major.minor to which the ".patch-version" will still be appended
USAGE
    exit 1
fi
values_file_path=${1:-}
input_chart_name=${2:-}
# https://stackoverflow.com/questions/50668585/bash-dash-case-to-camel-case
# this wizardry is required because (as of this comment) build-image is based upon busybox
# which does not have gnu-sed, and we have chosen not to use the python3 interpreter
values_key_in_file=$(echo "${input_chart_name}" | awk -F"-" '{for(i=2;i<=NF;i++){$i=toupper(substr($i,1,1)) substr($i,2)}} 1' OFS="")Version
patch_version=${3:-}
major_minor_pair=${4:-}


if [[ -z "$CHATOPS" ]]; then
    chat_start() {
        :
    }
    chat_stop() {
        :
    }
fi

if [[ ! -e "$values_file_path" ]]; then
    chat_start; echo "Sorry, your helmfile.d values file \"$values_file_path\" is 404" >&2; chat_stop
    exit 1
fi

if [[ -n "$CI" ]]; then
  echo "Putting the working copy back onto $CI_COMMIT_REF_NAME (was in detached head)" >&2
  git checkout --force "$CI_COMMIT_REF_NAME"
  git reset --hard "$CI_COMMIT_SHA"
fi

exit_if_not_dirty() {
  local fn="$1"
  if ! git status --porc "$fn" | grep -- "$fn"; then
    echo "Expected the change to dirty \"$fn\" but nope" >&2
    exit 1
  fi
}
if [[ -n "$major_minor_pair" ]]; then
  sed -i.bak -E -e 's/^('"${values_key_in_file}"': ).*/\1'"${major_minor_pair}.${patch_version}/" "$values_file_path"
else
  sed -i.bak -E -e 's/^('"${values_key_in_file}"': [[:digit:]]+[.][[:digit:]]+[.]).*/\1'"${patch_version}/" "$values_file_path"
fi
exit_if_not_dirty "$values_file_path"

git add "$values_file_path"
new_version=$(sed -nEe 's/^'"${values_key_in_file}"': //p' "$values_file_path")

if [[ -n "$CI" ]]; then
    # shellcheck disable=SC2001
    push_url="$(echo "$CI_REPOSITORY_URL" | sed -e "s/${CI_REGISTRY_USER}[^@]*@/oauth2:${GLR_PAT}@/")"
    git config --local user.name  "$GITLAB_USER_NAME"
    git config --local user.email "$GITLAB_USER_EMAIL"
    git remote set-url --push origin "$push_url"
    git commit -m"Bump Release of shared version $input_chart_name to $new_version

    By request of @${GITLAB_USER_LOGIN}
    "
    git push origin "$CI_COMMIT_REF_NAME" -o ci.skip
fi
