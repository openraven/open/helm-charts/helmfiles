#!/usr/bin/env bash
set -euo pipefail
CI="${CI:-}"
TRACE="${TRACE:-}"

MR_BRANCH="master"
NEW_SPRINT_BRANCH="release/$(date +"%Y-%m-%d")"

trace_on() {
    if [[ -n "$TRACE" ]]; then
      set -x
    fi
}
trace_on

# the first arg should be the RELATIVE path to API_V4_URL
gitlab_put() {
    set +x
    curl -X PUT -s -H "PRIVATE-TOKEN: ${GLR_PAT}" \
    -fo put.out "${CI_API_V4_URL}/${1}"
    trace_on

    if [[ -n "$TRACE" ]]; then
        cat put.out
    fi
}

if [[ -n "$CI" ]]; then
    # Setup git for CI
    set +x
    push_url="$(echo "$CI_REPOSITORY_URL" | sed -Ee "s/${CI_REGISTRY_USER}[^@]*@/oauth2:${GLR_PAT}@/")"
    git remote set-url --push origin "$push_url"
    trace_on
    git config --local user.name  "$GITLAB_USER_NAME"
    git config --local user.email "$GITLAB_USER_EMAIL"
fi

# Fetch needs to happen or none of the remote branches are present for subsequent steps
git fetch --force --all

# Ensure we always create the new branch from $MR_BRANCH
# Since we're playing with the $DEFAULT_BRANCH we need to be explicit
git switch "$MR_BRANCH"
git reset --hard "origin/$MR_BRANCH"

git branch "$NEW_SPRINT_BRANCH"

git commit --allow-empty -m "Starting $NEW_SPRINT_BRANCH"

# these options are from: https://docs.gitlab.com/ee/user/project/push_options.html#push-options-for-merge-requests
git push origin "$NEW_SPRINT_BRANCH" \
    -o merge_request.create \
    -o merge_request.target="$MR_BRANCH" \
    -o merge_request.remove_source_branch \
    -o merge_request.title="$NEW_SPRINT_BRANCH" \
    -o merge_request.description="$NEW_SPRINT_BRANCH" \
    -o merge_request.label="Release"

gitlab_put "projects/${CI_PROJECT_ID}/?default_branch=${NEW_SPRINT_BRANCH}"

chat_start
echo
echo "Created new sprint_branch $NEW_SPRINT_BRANCH and set as the Default Branch."
echo "Happy Development!"
chat_stop
