#! /usr/bin/env bash
set -euo pipefail
CI="${CI:-}"
HELMFILE_LOG_LEVEL="${HELMFILE_LOG_LEVEL:-debug}"
TRACE="${TRACE:-}"

[[ -n "$TRACE" ]] && set -x

CI_PROJECT_DIR="${CI_PROJECT_DIR:-$(git rev-parse --show-toplevel)}"
CHARTS_DIRECTORY="${CHARTS_DIRECTORY:-charts}"

cd "${CI_PROJECT_DIR}"

if ! type python3 >/dev/null 2>&1; then
    apk add python3 || { echo "python3 is required"; exit 1; }
fi

for i in ./"${CHARTS_DIRECTORY}"/*/Chart.yaml; do
  chart_dir=$(dirname "$i")
  # In CI, we should have already packaged these at the ${CI_PROJECT_DIR} level.
  # If they exist, cool, if not, package them. This allows it to work locally
  # and in CI without double work.
  chart_name=$(awk -F: '/^name:/{ gsub(" ", "", $2); print $2 }' "$i")
  chart_ver=$(awk -F: '/^version:/{ gsub(" ", "", $2); print $2 }' "$i")
  # it seems that "find" by itself is not exiting as expected, which is what the grep is for
  if ! find ./ -name "${chart_name}-${chart_ver}*.tgz" -exec ls -l '{}' ';' | grep -q '\.tgz'; then
    helm ${TRACE:+--debug} package "$chart_dir"
  fi
done

# This 'needs' to be a real chart repo otherwise it will cause the script to fail when
# helmfiles tries to add the repository
# be careful, the "$CI_DEFAULT_BRANCH" changes from release to release, but only "master" is the "ship-it" branch
if [[ "${CI_BUILD_REF_NAME:-}" == "master" ]]; then
  HELM_S3_BUCKET='openraven-deploy-rds'
else
  HELM_S3_BUCKET='openraven-deploy-staging-rds'
fi
export HELM_S3_BUCKET
curl -fsSLo index-upstream.yaml https://${HELM_S3_BUCKET}.s3.amazonaws.com/charts/index.yaml
# Index the local chart repo
helm repo index --merge index-upstream.yaml "${CI_PROJECT_DIR}"

# Serve the local chart repo
cd "${CI_PROJECT_DIR}"
python3 -m http.server 9090 &
python_pid=$!
sleep 1

cleanup() {
    set +e # Clean up as much as possible and tolerate failures
    # SO, it turns out "kill -INT" isn't strong enough to bring down -m http.server
    # but "kill -9" emits ugly errors, so only do that locally
    if [[ -z "$CI" ]]; then
      echo "Killing the local python http.server ..." >&2
      kill -9 ${python_pid}
    fi
    echo "Cleaning up local index.yaml, generated charts ..." >&2
    rm -rf index.yaml ./*.tgz
    # when running this locally but within docker, it will run as root
    # which is a PITA for the working copy
    if [[ -z "$CI" ]] && [[ -e /.dockerenv ]]; then
        find . -user 0 -exec chmod a+rwX '{}' '+'
    fi
}
trap cleanup EXIT

export HELM_S3_URL="http://127.0.0.1:9090/"
curl -fI "${HELM_S3_URL}/index.yaml"

# Environment Variables required for the test run to succeed.
export ENC_PASS4SYMMKEY='FOTvIwm6iDg13Cbxy9bPgSbZnl6NZCcYH5pn+v5qBhqTb44oXi5jv/A9yEQMlxc+'
export ENC_TELEPORT_TOKEN='KxutRbGzgmtCgWm/nMYjzFkVoOIkqpdoAfSe8CIUe5JvfhpeLht3A7i/+dvICYuC'
export ENC_DD_API_KEY='iiYAdXTck5JG3ERu72vr7p7yuq62p1CKEn3DtBQNBbI/IqV0YDRu2KKCOUHBWbOA'
export ENC_SCK_PRODUCT_K8S='vi4HSgfgbCT6eSRCc8serjnYbLsoi16o3Z2Sqf+m2XWfB7s4Qt5+hRyUEcyo6qfHI+MJPVtGDvS1ALSQhviEGg=='
export ENC_SCK_PRODUCT_K8S_EVENTS='Dg59Q5NqMDzyGYF8kVEWlrZrzF/KXcNQ/U/4MvUlnnN9T46cQv/c7ttxYlbM7ZwW/XzyFiA2xrA2O3ZvocP19g=='
export ENC_SCK_PRODUCT_K8S_METRICS='UwbUJIWD5LMScymag0cePT7WiyX1JkEfI23dteE3OsrErQlWtBfGqGXnWbitY8lwROepiw81PNAKfhVJyS/KOQ=='
export ENC_FAIRWINDS_INSIGHTS_TOKEN='bZTGMy8EgBd+VgLqFg3oUDu+NpywJg3cREkHvc/XxG6m0lZd3eIsOLe25XPykHqu'
export ENC_DSAL_HEC_TOKEN='zPkwDteuavniI7/BdBPvt7UfEFWMzBcxd87tMLeqCY4='
export ENC_TESTING_PASSWORD='lH+2BBh62btHmUo0nhds6S3SzC5h+0cyEuGAdkZDV3Q='
export ADMIN_CLIENT_ID='AdMiNCliE-nt-id'
export ADMIN_CLIENT_SECRET='ZeAdmen/Sekrit-123'
export COOKIE_SECRET='c00kieS3kritmustbec00kieS3kritAA'
CLUSTER_NAME=${CLUSTER_NAME:-'Spaces Are Awesome!'}
export CLUSTER_NAME
export FRONTEND_CLIENT_ID='feCID'
export GROUP_ID='ThEgRoUpId'
export OPENRAVEN_INGRESS_HOSTNAME='www.example.com'
export SERVICE_CLIENT_ID='seCLID'
export SERVICE_CLIENT_SECRET='scSec'
export OPENRAVEN_UNSTABLE_UNRELEASED_FEATURES='true'
export OPENRAVEN_UNRELEASED_FEATURES='true'
# watch out, we do hostname trickery with this in the teleport helmfile so don't alter it willy nilly
export SQITCH_TARGET='postgresql://orvn-pgstack.cluster-cafebabe.us-west-2.rds.amazonaws.com:5432/orvn'
export SPLUNK_BUCKET='orvn-splunk-bucket'
export JDBC_SECRET_NAME='jdbc-env'
# Enable data-accuracy-testing chart so we ensure that it templates correctly
export ACCURACY_TESTING=1
# enable snowflake to test the snowflake chart
export SNOWFLAKE_ENABLED="true"
# enable splunk to validate testing
export SPLUNK_ENABLED="true"

export ACM_CERT_ARN='arn:aws:acm:us-west-2:012345678901:certificate/1ab5c35d-69e3-40d8-aa05-0914de00f654'

# This enables the demo profiles across the various services
export DEMO_ENV_ENABLED=1

HELMFILE_GIT_URL="file://$PWD"
HELMFILE_GIT_REF="$(git rev-parse HEAD)"
export HELMFILE_GIT_URL
export HELMFILE_GIT_REF
if ! helmfile --log-level "${HELMFILE_LOG_LEVEL}" -f helmfile.yaml template --output-dir=/tmp/k8s; then
    echo 'the rendered values files: ' >&2
    cat /tmp/values* || true
    exit 1
fi
if command -v kubeconform >/dev/null 2>&1; then
  if ! kubeconform -ignore-missing-schemas -strict -verbose /tmp/k8s; then
      # make it more obvious why the run failed,
      # since our kubeconform friend hides failures in a sea of text
      echo 'EGAD, kubeconform did not shake out' >&2
      exit 1
  fi
else
  echo "Skipping Validation. 'kubeconform' not present."
  echo "Install 'kubeconform'! (https://github.com/yannh/kubeconform)"
fi
if [[ -n "${SHOW_K8S:-}" ]]; then
    echo "# the files from ACM_CERT_ARN=$ACM_CERT_ARN" >&2
    for d in /tmp/k8s/*; do
        bn="$(basename "$d")"
        echo "# $bn {{{"
        find "$d" -type f -exec cat '{}' ';'
        echo "# $bn }}}"
    done
    echo "# END files from ACM_CERT_ARN=$ACM_CERT_ARN" >&2
fi
rm -rf /tmp/k8s
echo "Everything is OK"
