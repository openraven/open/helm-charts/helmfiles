#! /usr/bin/env bash
set -euo pipefail
TRACE="${TRACE:-}"
curl_command="curl"

if [[ -n "$TRACE" ]]; then
  set -x
  curl_command="$curl_command -v "
fi

if [[ -z "${SLACK_WEBHOOK_URL:-}" ]]; then
    # shellcheck disable=SC2016
    echo 'Not without $SLACK_WEBHOOK_URL' >&2
    exit 1
fi

message="${1}"

slack_payload() {
  jq --arg text "$message"  -n '{text: $text}'
}

$curl_command -fsH 'content-type: application/json;charset=utf-8' \
     --data-binary "$(slack_payload)" \
     "$SLACK_WEBHOOK_URL"
