{{ $s3Bucket       := requiredEnv "HELM_S3_BUCKET" -}}
{{ $s3BucketHost   := env "HELM_S3_BUCKET_HOST" | default "s3.amazonaws.com" }}
{{ $ourUrl         := env "HELM_S3_URL"         | default (printf "https://%s.%s/charts" $s3Bucket $s3BucketHost) }}
{{ $groupId        := requiredEnv "GROUP_ID" }}
{{ $clusterName    := requiredEnv "CLUSTER_NAME" }}
{{ $jdbcEnvName    := env "JDBC_SECRET_NAME"    | default "jdbc-env" }}
{{ $name           := "cloudtrail-ingestion" }}
{{ $serviceEnabled := not (env "DDR_ENABLED" | empty) }}
{{ $image          := (readFile (printf "./values/%s-image.yaml" $name) | fromYaml) }}
{{ $chart          := (readFile "./values/openraven-java-spring-version.yaml" | fromYaml) }}
{{ $acmCertArn     := env "ACM_CERT_ARN" }}
{{ $awsAccountId   := regexReplaceAll "arn:aws:acm:[^:]+:([0-9]+):certificate.*" $acmCertArn "$1" }}
{{ $ddrEnabled     := env "DDR_ENABLED" | default "false" }}

repositories:
  - name: openraven
    url: {{ $ourUrl }}

releases:
  - name: {{ $name }}
    installed: {{ $serviceEnabled }}
    namespace: ui
    createNamespace: true
    chart: openraven/openraven-java-spring
    version: {{ $chart.version }}
    wait: false
    values:
      - image:
          repository: 160390125595.dkr.ecr.us-west-2.amazonaws.com/openraven/openraven/cloudtrail-ingestion
          tag: {{ $image.tag | quote }}
        kiamRole: cross-account
        kiamAccountId: {{ $awsAccountId | quote }}
        spring:
          profilesActive: default, prod, aws
          datasource:
            hikari:
              maximumPoolSize: 5
              minimumIdle: 1
        openraven:
          clusterName: {{ $clusterName }}
          groupId: {{ $groupId }}
        sentry:
          enabled: {{ (tpl (readFile "./values/sentryEnabled.gotmpl") (dict "clusterName" $clusterName "s3Bucket" $s3Bucket) ) }}
          dsn: https://d4e4e782750a44aeaa0145b181af138e@o322024.ingest.sentry.io/4505513077833728
        resources:
          requests:
            memory: 2Gi
          limits:
            memory: 4Gi
        extraEnv:
          - name: DDR_ENABLED
            value: {{ $ddrEnabled | quote }}
        envFrom:
          - secretRef:
              name: {{ $jdbcEnvName }}
      - ./values/database-schema-version.yaml
      - {{ .Values | toJson }}

  - name: {{ $name }}-gcp
    installed: {{ $serviceEnabled }}
    namespace: ui
    createNamespace: true
    chart: openraven/openraven-java-spring
    version: {{ $chart.version }}
    wait: false
    values:
      - image:
          repository: 160390125595.dkr.ecr.us-west-2.amazonaws.com/openraven/openraven/cloudtrail-ingestion
          tag: {{ $image.tag | quote }}
        kiamRole: cross-account
        kiamAccountId: {{ $awsAccountId | quote }}
        spring:
          profilesActive: default, prod, gcp
          datasource:
            hikari:
              maximumPoolSize: 5
              minimumIdle: 1
        openraven:
          clusterName: {{ $clusterName }}
          groupId: {{ $groupId }}
        sentry:
          enabled: {{ (tpl (readFile "./values/sentryEnabled.gotmpl") (dict "clusterName" $clusterName "s3Bucket" $s3Bucket)) }}
          dsn: https://d4e4e782750a44aeaa0145b181af138e@o322024.ingest.sentry.io/4505513077833728
        resources:
          requests:
            memory: 2Gi
          limits:
            memory: 4Gi
        extraEnv:
          - name: DDR_ENABLED
            value: {{ $ddrEnabled | quote }}
        envFrom:
          - secretRef:
              name: {{ $jdbcEnvName }}
      - ./values/database-schema-version.yaml
      - {{ .Values | toJson }}
