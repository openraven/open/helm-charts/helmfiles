{{ $tagJson := env "CLUSTER_TAGS_JSON" }}
{{ $groupId := requiredEnv "GROUP_ID" }}
{{ $acmCertArn := env "ACM_CERT_ARN" }}
{{/* e.g. "arn:aws:acm:us-west-2:012345678901:certificate/1ab5c35d-69e3-40d8-aa05-0914de00f654" */}}
{{ $awsAccountId    := regexReplaceAll "arn:aws:acm:[^:]+:([0-9]+):certificate.*" $acmCertArn "$1" }}
{{ $csiDriverRoleName := printf "orvn-%s-csi-driver" $groupId }}
{{ $csiDriverRoleArn := empty $acmCertArn | ternary "" (printf "arn:aws:iam::%s:role/%s" $awsAccountId $csiDriverRoleName) }}

repositories:
  - name: aws-ebs-csi-driver
    url: https://kubernetes-sigs.github.io/aws-ebs-csi-driver

releases:
- name: aws-ebs-csi-driver
  namespace: kube-system
  chart: aws-ebs-csi-driver/aws-ebs-csi-driver
  # https://github.com/kubernetes-sigs/aws-ebs-csi-driver/releases | grep helm-chart
  version: 2.29.1
  wait: true
  values:
  - enableVolumeScheduling: "true"
    enableVolumeResizing: "true"
    enableVolumeSnapshot: "true"
    # https://github.com/kubernetes-sigs/aws-ebs-csi-driver/blob/v0.6.0/aws-ebs-csi-driver/values.yaml#L65-L70
    {{ if $tagJson }}
    {{/*
       we're cheating here to take advantage of the fact that YAML is a superset of JSON
       since helmfile has no "fromJson" like a sane system :-( */}}
    extraVolumeTags:
{{ $tagJson | fromYaml | toYaml | indent 6 }}
    {{ end }}
    resources:
      limits:
        memory: 256M
      requests:
        memory: 64M
    controller:
      k8sTagClusterId: orvn-{{ $groupId }}
      {{ if $csiDriverRoleArn }}
      env:
      - name: AWS_ROLE_ARN
        value: {{ $csiDriverRoleArn | quote }}
      - name: AWS_WEB_IDENTITY_TOKEN_FILE
        value: /var/run/secrets/eks.amazonaws.com/serviceaccount/token
      serviceAccount:
        annotations:
          eks.amazonaws.com/role-arn: {{ $csiDriverRoleArn }}
      {{ else }}
      nodeSelector:
        node-role.kubernetes.io/master: ""
      tolerations:
      - effect: NoSchedule
        operator: Exists
      {{ end }}
    node:
      {{ if $csiDriverRoleArn }}
      env:
      - name: AWS_ROLE_ARN
        value: {{ $csiDriverRoleArn | quote }}
      - name: AWS_WEB_IDENTITY_TOKEN_FILE
        value: /var/run/secrets/eks.amazonaws.com/serviceaccount/token
      serviceAccount:
        annotations:
          eks.amazonaws.com/role-arn: {{ $csiDriverRoleArn }}
      {{ end }}
      # the **node** needs to run everywhere, even potentially on the Splunk Indexers
      # https://github.com/kubernetes-sigs/aws-ebs-csi-driver/issues/848
      tolerations:
      - effect: NoSchedule
        operator: Exists
    storageClasses:
    # it seems this name is hard-coded somewhere
    - name: default-storage-class
      annotations:
        storageclass.kubernetes.io/is-default-class: "true"
      allowVolumeExpansion: true
      parameters:
        encrypted: "true"
        type: gp3
  - {{ .Values | toJson }}
