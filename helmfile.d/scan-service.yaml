{{ $s3Bucket          := requiredEnv "HELM_S3_BUCKET" -}}
{{ $s3BucketHost      := env "HELM_S3_BUCKET_HOST" | default "s3.amazonaws.com" }}
{{ $ourUrl            := env "HELM_S3_URL"         | default (printf "https://%s.%s/charts" $s3Bucket $s3BucketHost) }}
{{ $groupId           := requiredEnv "GROUP_ID" }}
{{ $ingHostname       := requiredEnv "OPENRAVEN_INGRESS_HOSTNAME" }}
{{ $name              := "scan-service" }}
{{ $image             := (readFile (printf "./values/%s-image.yaml" $name) | fromYaml) }}
{{ $clusterName       := requiredEnv "CLUSTER_NAME" }}
{{ $adminClientSecret := requiredEnv "ADMIN_CLIENT_SECRET" }}
{{ $adminClientId     := requiredEnv "ADMIN_CLIENT_ID"}}
{{ $demoEnvEnabled    := env "DEMO_ENV_ENABLED" }}
{{ $springProfiles    := "default, prod" }}
{{ if $demoEnvEnabled }}
{{ $springProfiles = print $springProfiles ", demo" }}
{{ end }}
{{ $jdbcEnvName       := env "JDBC_SECRET_NAME"    | default "jdbc-env"}}
{{ $acmCertArn        := env "ACM_CERT_ARN" }}
{{/* e.g. "arn:aws:acm:us-west-2:012345678901:certificate/1ab5c35d-69e3-40d8-aa05-0914de00f654" */}}
{{ $awsAccountId      := regexReplaceAll "arn:aws:acm:[^:]+:([0-9]+):certificate.*" $acmCertArn "$1" }}
{{ $chart             := (readFile "./values/openraven-java-spring-version.yaml" | fromYaml) }}

repositories:
- name: openraven
  url: {{ $ourUrl }}

releases:
- name: {{ $name }}-api
  namespace: dmap
  createNamespace: true
  chart: openraven/openraven-java-spring
  version: {{ $chart.version }}
  installed: true
  values:
  - image:
      repository: 160390125595.dkr.ecr.us-west-2.amazonaws.com/openraven/openraven/scan-service
      tag: {{ $image.tag | quote }}
    openraven:
      clusterName: {{ $clusterName }}
      groupId: {{ $groupId }}
    spring:
      profilesActive: {{ $springProfiles }}, api
      datasource:
        hikari:
          maximumPoolSize: 5
          minimumIdle: 1
    service:
      enabled: true
    startupProbe:
      httpGet:
        path: /actuator/health
        port: management
      failureThreshold: 10
      periodSeconds: 15
    resources:
      limits:
        memory: 2Gi
      requests:
        memory: 1Gi
    sentry:
      enabled: {{ (tpl (readFile "./values/sentryEnabled.gotmpl") (dict "clusterName" $clusterName "s3Bucket" $s3Bucket) ) }}
      dsn: https://eea6b3c7bbcb4b9cbbdfccef714d42cd@o322024.ingest.sentry.io/5406671
    extraEnv:
    - name: SPRING_SECURITY_OAUTH2_CLIENT_REGISTRATION_ASGARD_CLIENT-ID
      value: {{ $adminClientId }}
    - name: SPRING_SECURITY_OAUTH2_CLIENT_REGISTRATION_ASGARD_CLIENT-SECRET
      value: {{ $adminClientSecret }}
    - name: OPENRAVEN_APP_V1_CLUSTER_NAME
      value: {{ $clusterName | quote }}
    - name: OPENRAVEN_APP_V1_CLUSTER_HOST
      value: {{ $ingHostname }}
    ingress:
      enabled: true
      annotations:
        konghq.com/plugins: oidc,x-openraven-user
      hosts:
      - host: {{ $ingHostname }}
        paths:
        - path: "/api/data-identifiers"
        - path: "/api/s3scan"
        - path: "/api/v2/s3scan"
        - path: "/api/scanner-jobs"
        - path: "/api/scanner-job-runs"
        - path: "/api/sdss/"
    externalIngress:
      enabled: true
      hosts:
      - host: {{ $ingHostname }}
        paths:
        - path: "/~/external(/api/scans/v1/s3/.*)"
        - path: "/~/external(/api/scans/v1/grive/.*)"
        - path: "/~/external(/api/data-classification/v1/data-.*)"
        - path: "/~/external(/api/sdss/.*)"
        - path: "/~/external(/api/scans/v1/sdss/.*)"
    envFrom:
    - secretRef:
        name: {{ $jdbcEnvName }}
    - secretRef:
        name: redis
  - {{ .Values | toJson }}

- name: {{ $name }}-svc
  namespace: dmap
  createNamespace: true
  chart: openraven/openraven-java-spring
  version: {{ $chart.version }}
  installed: true
  values:
  - datadog:
      integrations:
        prometheus:
          maxReturnedMetrics: 10000
    azure:
      enabled: true
    image:
      repository: 160390125595.dkr.ecr.us-west-2.amazonaws.com/openraven/openraven/scan-service
      tag: {{ $image.tag | quote }}
    openraven:
      clusterName: {{ $clusterName }}
      groupId: {{ $groupId }}
    # Created in helmfile.d/azure-discovery-svc-sa.yaml
    serviceAccount:
      create: false
      name: azure-discovery-svc
    spring:
      profilesActive: {{ $springProfiles }}, service
      datasource:
        hikari:
          maximumPoolSize: 30
          minimumIdle: 5
    service:
      enabled: true
    startupProbe:
      httpGet:
        path: /actuator/health
        port: management
      failureThreshold: 10
      periodSeconds: 15
    resources:
      limits:
        memory: 4Gi
      requests:
        memory: 2Gi
    sentry:
      enabled: {{ (tpl (readFile "./values/sentryEnabled.gotmpl") (dict "clusterName" $clusterName "s3Bucket" $s3Bucket) ) }}
      dsn: https://eea6b3c7bbcb4b9cbbdfccef714d42cd@o322024.ingest.sentry.io/5406671
    extraEnv:
    - name: SPRING_SECURITY_OAUTH2_CLIENT_REGISTRATION_ASGARD_CLIENT-ID
      value: {{ $adminClientId }}
    - name: SPRING_SECURITY_OAUTH2_CLIENT_REGISTRATION_ASGARD_CLIENT-SECRET
      value: {{ $adminClientSecret }}
    - name: OPENRAVEN_APP_V1_CLUSTER_NAME
      value: {{ $clusterName | quote }}
    - name: OPENRAVEN_APP_V1_CLUSTER_HOST
      value: {{ $ingHostname }}
    - name: OPENRAVEN_APP_V1_CLOUD-INGESTION_ANALYTICS_CLUSTERID
      value: {{ $groupId }}
    envFrom:
    - secretRef:
        name: {{ $jdbcEnvName }}
    - secretRef:
        name: redis
  - {{ .Values | toJson }}
